
Full step by step functionality for oanda service to fetch all data for customer and respective accounts and rates of instruments

    $oandaService
        ->initUser($thisUser)
        ->initOandaV20()
        ->fetchCurrentUserAccounts()
        ->getAllAccountSummaryForCurrentUser()
        ->importAllTradingInstruments()
        ->getInstrumentRates()
    ;