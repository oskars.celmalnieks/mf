<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use AppBundle\Service\OandaOperationsService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OandaRatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:fetch-rates')
            ->setDescription('get last rates starting from')
            ->addArgument('date-start', InputArgument::REQUIRED, 'date yyyy-mm-dd', null)
            ->addOption('count', null, InputOption::VALUE_OPTIONAL, 'count', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $doctrine = $this->getContainer()->get('doctrine')->getManager();
        $userRepo = $doctrine->getRepository(User::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        /** @var OandaOperationsService $oandaService */
        $oandaService = $this->getContainer()->get(OandaOperationsService::class);

        $argument = $input->getArgument('date-start');

        if (\DateTime::createFromFormat('Y-m-d', $argument) == FALSE) {

            echo "Input datetime string not valid".PHP_EOL;
            return;
        }

        $startDate = new \DateTime($argument);
        $dateNow = new \DateTime('now');

        do {

            $theDate = $startDate->format(DATE_ATOM);

            $data = [
                'price' => 'MBA',
                'granularity' => 'M1',
                'from' => $theDate,
                'count' => 5000,
            ];

            echo "Requesting rates for 3 days starting from: ".$theDate.PHP_EOL;

            $oandaService
                ->initUser($thisUser)
                ->initOandaV20()
                ->getInstrumentRates($data)
            ;

            $startDate->modify('+3days');
            $this->getContainer()->get('doctrine')->getManager()->clear();

        } while ($dateNow > $startDate);

        echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>".PHP_EOL;
        echo ">>>>> Dates fetching finished".PHP_EOL;
        echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>".PHP_EOL;

    }
}
