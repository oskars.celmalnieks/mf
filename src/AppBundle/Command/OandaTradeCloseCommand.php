<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\Trade;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountDataRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\InstrumentRepository;
use AppBundle\Repository\RateRepository;
use AppBundle\Repository\TradeRepository;
use AppBundle\Service\OandaOperationsService;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use TheCodeMill\OANDA\OANDAv20;

class OandaTradeCloseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:trade-close')
            ->setDescription('trade close execute')
            ->addArgument('instrument_data', InputArgument::IS_ARRAY, 'instrument long/short tradeId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $instrument = $input->getArgument('instrument_data')[0];
        $side = $input->getArgument('instrument_data')[1];
        $tradeNo = $input->getArgument('instrument_data')[2];

        if(is_null($instrument) || is_null($tradeNo) || is_null($side)) {
            $message = "Instrument and/or trade and/or side no not set in command";

            return $message;
        }

        $sideOptions = [
            'long',
            'short'
        ];

        if(!in_array($side, $sideOptions)) {
            $message = "Side not chosen correctly: options available = [long / short]".PHP_EOL;
            echo $message;

            return $message;
        }

        $doctrine = $this->getContainer()->get('doctrine')->getManager();

        /** @var InstrumentRepository $instrumentRepo */
        $instrumentRepo = $doctrine->getRepository(Instrument::class);

        /** @var Instrument $thisInstrument */
        $thisInstrument = $instrumentRepo->findOneBy(
            [
                'name' => trim(strtoupper($instrument))
            ]
        );

        if (is_null($thisInstrument)) {
            $thisInstrument = $instrumentRepo->findOneBy(
                [
                    'name' => Instrument::DEFAULT_INSTRUMENT_NAME
                ]
            );
        }

        $side == 'long' ? $side = Account::SIDE_LONG : $side = Account::SIDE_SHORT;

        $userRepo = $doctrine->getRepository(User::class);

        /** @var TradeRepository $tradeRepo */
        $tradeRepo = $doctrine->getRepository(Trade::class);

        /** @var AccountRepository $accountRepo */
        $accountRepo = $doctrine->getRepository(Account::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        /** @var OandaOperationsService $oandaService */
        $oandaService = $this->getContainer()->get(OandaOperationsService::class);

        /** @var Account $accountToOperate */
        $accountToOperate = $accountRepo->findOneBy(
            [
                'user' => $thisUser,
                'side' => $side,
                'enabled' => true,
                'instrument' => $thisInstrument
            ]
        );

        $thisTrade = $tradeRepo->findOneBy(['oandaTradeId' => $tradeNo, 'account' => $accountToOperate]);

        if(is_null($thisTrade)) {
            $message = "Trade account not found for indicated account".PHP_EOL;
            echo $message;

            return $message;
        }

        $oandaService
            ->initUser($thisUser)
            ->initOandaV20()
            ->closeTrade($accountToOperate, $thisTrade);
        ;

        echo "FINISHED".PHP_EOL;
    }
}
