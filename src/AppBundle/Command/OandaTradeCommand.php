<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\Trade;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountDataRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\InstrumentRepository;
use AppBundle\Repository\RateRepository;
use AppBundle\Service\OandaOperationsService;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use TheCodeMill\OANDA\OANDAv20;

class OandaTradeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:trade-open')
            ->setDescription('trade order execute')
            ->addArgument('instrument_data', InputArgument::IS_ARRAY, 'instrument')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ARGUMET_COUNT = 3;

        if(count($input->getArgument('instrument_data')) != $ARGUMET_COUNT) {
            $message = "Instrument and/or amount and/or side not set in command";
            echo $message;

            return $message;
        }

        $instrument = $input->getArgument('instrument_data')[0];
        $units      = $input->getArgument('instrument_data')[1];
        $side       = strtoupper($input->getArgument('instrument_data')[2]);

        if(is_null($instrument) || is_null($units) || is_null($side)) {
            $message = "Instrument and/or amount not set in command";
            echo $message;

            return $message;
        }

        $sideOptions = [
            'BUY',
            'SELL'
        ];

        if(!in_array($side, $sideOptions)) {
            $message = "Side not chosen correctly: options available = [BUY / SELL]".PHP_EOL;
            echo $message;

            return $message;
        }

        $doctrine = $this->getContainer()->get('doctrine')->getManager();
        $userRepo = $doctrine->getRepository(User::class);

        /** @var User $thisUser */
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        /** @var OandaOperationsService $oandaService */
        $oandaService = $this->getContainer()->get(OandaOperationsService::class);

        if($side == "SHORT") {
            $units = -$units;
        }

        $oandaService
            ->initUser($thisUser)
            ->initOandaV20()
            ->openTrade($instrument, $units);
        ;

        echo "FINISHED".PHP_EOL;
    }
}
