<?php

namespace AppBundle\Command;

use AppBundle\Entity\Rate;
use AppBundle\Repository\RateRepository;
use AppBundle\Service\SimulateTradingQuestService;
use AppBundle\Service\SimulateTradingService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OandaSimulatorQuestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:simulator-quest')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {
            // ...
        }

        $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $years = ['2009','2013','2014','2015','2016','2017','2018'];

//        $month = ['11'];
        $years = ['2018'];

        $grandTotal = 0;

        foreach ($years as $year) {

            $yearGrandTotal = 0;
            $yearGrandTrades = 0;

            foreach ($month as $thisMonth) {

                $startDate = "{$year}-{$thisMonth}-01";
                $endTime = "{$year}-{$thisMonth}-31";

                $startDateObject = new \DateTime($startDate);
                $endDateObject = new \DateTime($endTime);

                $em = $this->getContainer()->get('doctrine')->getManager();

                /** @var RateRepository $ratesRepo */
                $ratesRepo = $em->getRepository(Rate::class);
                $rates = $ratesRepo->getListOfRatesDescendingByMoment($startDateObject, $endDateObject);

                $instrumentSpecifics = [
                    'maCount' => 91,
                    'spread' => 2,
                    'profitRatio' => 2,
                    'profit' => 0.0018,
                    'loss' => 0.0018,
                    'units' => 10000,
                    'volume' => 400,
                ];

                $dataToPass = [
                    'units' => $instrumentSpecifics['units'],
                    'instrument' => 'EUR_USD',
                    'ratesObjectArray' => $rates,
                    'instrumentSpecifics' => $instrumentSpecifics
                ];

                $limits = [
                    'spreadProfit' => 1.0055,
                    'spreadLoss' => 1.0055,
                ];

                //TODO: get array of rates ordered desc by moment

                /** @var SimulateTradingQuestService $simulator */
                $simulator = $this->getContainer()->get('AppBundle\Service\SimulateTradingQuestService');
                $simulator->init($dataToPass, $limits);

                $commonResult = $simulator->seeCommonResult(true);

                $grandTotal += $commonResult['subTotal'];

//        $thisWeek = (int) $data['week']['week'];

                $resultOutput = sprintf('%s : subTotal %d : tradeCount %d',
                    "{$year}-{$thisMonth}-01",
                    $commonResult['subTotal'],
                    count($commonResult['tradeFlow'])
//                $lastWeekBest->getTp(),
//                $lastWeekBest->getSl()
                );

                $yearGrandTotal += $commonResult['subTotal'];
                $yearGrandTrades += count($commonResult['tradeFlow']);

                dump($commonResult['tradeFlow']);
                dump($resultOutput);
                $this->getContainer()->get('doctrine')->getManager()->clear();

//        $output->writeln($commonResult);
            }

            $yearGrandTotalMessage = sprintf('year GRAND TOTAL = %d ; GRAND TOTAL TRADES COUNT = %d ; AVERAGE TRADE COUNT/MO = %f', $yearGrandTotal, $yearGrandTrades, ($yearGrandTrades/12));
            dump($yearGrandTotalMessage);

            $this->getContainer()->get('doctrine')->getManager()->clear();
        }

        dump(sprintf('GRAND TOTAL = %d', $grandTotal));

        $output->writeln('======= FINISHED ======.');
    }

}
