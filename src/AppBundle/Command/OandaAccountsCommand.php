<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountDataRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\InstrumentRepository;
use AppBundle\Repository\RateRepository;
use AppBundle\Service\OandaOperationsService;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OandaAccountsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:fetch-accounts')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $doctrine = $this->getContainer()->get('doctrine')->getManager();
        $userRepo = $doctrine->getRepository(User::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        /** @var OandaOperationsService $oandaService */
        $oandaService = $this->getContainer()->get(OandaOperationsService::class);

        $oandaService
            ->initUser($thisUser)
            ->initOandaV20()
            ->fetchCurrentUserAccounts()
            ->getAllAccountSummaryForCurrentUser()
        ;
    }



}
