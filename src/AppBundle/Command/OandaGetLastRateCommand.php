<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountDataRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\InstrumentRepository;
use AppBundle\Repository\RateRepository;
use AppBundle\Service\OandaOperationsService;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use TheCodeMill\OANDA\OANDAv20;

class OandaGetLastRateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:fetch-last-rate')
            ->setDescription('Last 1 min rate')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $doctrine = $this->getContainer()->get('doctrine')->getManager();
        $userRepo = $doctrine->getRepository(User::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        /** @var OandaOperationsService $oandaService */
        $oandaService = $this->getContainer()->get(OandaOperationsService::class);

        $data = [
            'count' => 2,
            'price' => 'MBA',
            'granularity' => 'M1',
        ];

        $oandaService
            ->initUser($thisUser)
            ->initOandaV20()
            ->getInstrumentRates($data)
        ;
    }



}
