<?php

namespace AppBundle\Command;

use AppBundle\Entity\Rate;
use AppBundle\Repository\RateRepository;
use AppBundle\Service\SimulateTradingService;
use AppBundle\Service\SimulateTradingVirtualService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OandaSimulatorVirtualCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:simulator-virtual')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var RateRepository $ratesRepo */
        $ratesRepo = $em->getRepository(Rate::class);
        $rates = $ratesRepo->getListOfLastThreeRatesDescendingByMoment();

        $instrumentSpecifics = [
            'maCount' => 91,
            'spread' => 2,
            'profitRatio' => 2,
            'profit' => 0.0018,
            'loss' => 0.0018,
            'units' => 10000,
            'volume' => 400,
        ];

        $dataToPass = [
            'units' => $instrumentSpecifics['units'],
            'instrument' => 'EUR_USD',
            'ratesObjectArray' => $rates,
            'instrumentSpecifics' => $instrumentSpecifics,
            'scenario' => 77,
        ];

        $limits = [
            'spreadProfit' => 1.0055,
            'spreadLoss' => 1.0055,
        ];

        //TODO: get array of rates ordered desc by moment

        /** @var SimulateTradingVirtualService $simulator */
        $simulator = $this->getContainer()->get('AppBundle\Service\SimulateTradingVirtualService');
        $simulator->init($dataToPass, $limits);

        $simulator->execute(true);


    }
}
