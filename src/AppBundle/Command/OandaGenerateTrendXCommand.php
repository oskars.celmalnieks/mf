<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OandaGenerateTrendXCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('oanda:generate-trend-x')
            ->setDescription('...')
            ->addArgument('maCount', InputArgument::REQUIRED, 'maCount')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maCount = $input->getArgument('maCount');

        $generator = $this->getContainer()->get('AppBundle\Service\GenerateMissingWmaService');
        $generator->setMaCount($maCount);
//        $generator->updateMissgin();
        $generator->updateMissginTrendDirection();

//        if ($input->getOption('option')) {
//             ...
//        }

        $output->writeln('Command result.');
    }

}
