<?php
/**
 * Created by PhpStorm.
 * User: oskars
 * Date: 18.2.10
 * Time: 14:14
 */

namespace AppBundle\EventListener;

use Avanzu\AdminThemeBundle\Model\MenuItemModel;
use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use Symfony\Component\HttpFoundation\Request;

class MenuItemListListener {

    public function onSetupMenu(SidebarMenuEvent $event) {

        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }

    }

    protected function getMenu(Request $request) {
        // Build your menu here by constructing a MenuItemModel array
        $menuItems = [
            $blog = new MenuItemModel(1, 'Trades', 'list_trades', array(/* options */), 'iconclasses fa fa-book'),
            $blog = new MenuItemModel(2, 'Compare', 'compare_trades', array(/* options */), 'iconclasses fa fa-history'),
            $blog = new MenuItemModel(3, 'Logout', 'fos_user_security_logout', array(/* options */), 'iconclasses fa fa-beer'),

//            new MenuItemModel('1', 'Item', 'item_route_name'),
        ];

        // Add some children

        // A child with an icon
//        $blog->addChild(new MenuItemModel('ChildOneItemId', 'openTrades', 'list-trades', array(), 'fa fa-rss-square'));

        // A child with default circle icon
//        $blog->addChild(new MenuItemModel('ChildTwoItemId', 'ChildTwoDisplayName', 'child_2_route'));

        return $this->activateByRoute($request->get('_route'), $menuItems);
    }

    protected function activateByRoute($route, $items) {

        foreach($items as $item) {
            if($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            }
            else {
                if($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
            }
        }

        return $items;
    }

}