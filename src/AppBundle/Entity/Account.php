<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountRepository")
 */
class Account
{
    const SIDE_NONE = 0;
    const SIDE_LONG = 1;
    const SIDE_SHORT = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="oanda_id", type="string", length=100, unique=true)
     */
    private $oandaId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="accounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Instrument
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Instrument")
     * @ORM\JoinColumn(nullable=true)
     */
    private $instrument;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="side", type="integer")
     */
    private $side;

    /**
     * @var integer
     *
     * @ORM\Column(name="units", type="integer")
     */
    private $units;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AccountData", inversedBy="account")
     */
    protected $accountData;

    /**
     * @return AccountData
     */
    public function getAccountData()
    {
        return $this->accountData;
    }

    /**
     * @param mixed $accountData
     */
    public function setAccountData($accountData): void
    {
        $this->accountData = $accountData;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oandaId
     *
     * @param string $oandaId
     *
     * @return Account
     */
    public function setOandaId($oandaId)
    {
        $this->oandaId = $oandaId;

        return $this;
    }

    /**
     * Get oandaId
     *
     * @return string
     */
    public function getOandaId()
    {
        return $this->oandaId;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Account
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Account
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set side
     *
     * @param integer $side
     *
     * @return Account
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }

    /**
     * Get side
     *
     * @return integer
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * @return Instrument
     */
    public function getInstrument(): Instrument
    {
        return $this->instrument;
    }

    /**
     * @param Instrument $instrument
     */
    public function setInstrument(Instrument $instrument): void
    {
        $this->instrument = $instrument;
    }

    /**
     * @return int
     */
    public function getUnits(): int
    {
        return $this->units;
    }

    /**
     * @param int $units
     */
    public function setUnits(int $units): self
    {
        $this->units = $units;

        return $this;
    }
}

