<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\CreatedUpdatedTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Instrument
 *
 * @ORM\Table(name="instrument")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InstrumentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Instrument
{
    use CreatedUpdatedTrait;

    const DEFAULT_INSTRUMENT_NAME = "EUR_USD";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=10, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=10, unique=true)
     */
    private $displayName;

    /**
     * @var int
     *
     * @ORM\Column(name="pip_location", type="integer")
     */
    private $pipLocation;

    /**
     * @var int
     *
     * @ORM\Column(name="display_precision", type="integer")
     */
    private $displayPrecision;

    /**
     * @var int
     *
     * @ORM\Column(name="trade_units_precision", type="integer")
     */
    private $tradeUnitsPrecision;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_trade_size", type="float")
     */
    private $minimumTradeSize;

    /**
     * @var float
     *
     * @ORM\Column(name="maximum_trailing_stop_distance", type="float")
     */
    private $maximumTrailingStopDistance;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_trailing_stop_distance", type="float")
     */
    private $minimumTrailingStopDistance;

    /**
     * @var float
     *
     * @ORM\Column(name="maximum_position_size", type="float")
     */
    private $maximumPositionSize;

    /**
     * @var float
     *
     * @ORM\Column(name="maximum_order_units", type="float")
     */
    private $maximumOrderUnits;

    /**
     * @var float
     *
     * @ORM\Column(name="margin_rate", type="float")
     */
    private $marginRate;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Instrument
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Instrument
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return Instrument
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set pipLocation
     *
     * @param integer $pipLocation
     *
     * @return Instrument
     */
    public function setPipLocation($pipLocation)
    {
        $this->pipLocation = $pipLocation;

        return $this;
    }

    /**
     * Get pipLocation
     *
     * @return int
     */
    public function getPipLocation()
    {
        return $this->pipLocation;
    }

    /**
     * Set displayPrecision
     *
     * @param integer $displayPrecision
     *
     * @return Instrument
     */
    public function setDisplayPrecision($displayPrecision)
    {
        $this->displayPrecision = $displayPrecision;

        return $this;
    }

    /**
     * Get displayPrecision
     *
     * @return int
     */
    public function getDisplayPrecision()
    {
        return $this->displayPrecision;
    }

    /**
     * Set tradeUnitsPrecision
     *
     * @param integer $tradeUnitsPrecision
     *
     * @return Instrument
     */
    public function setTradeUnitsPrecision($tradeUnitsPrecision)
    {
        $this->tradeUnitsPrecision = $tradeUnitsPrecision;

        return $this;
    }

    /**
     * Get tradeUnitsPrecision
     *
     * @return int
     */
    public function getTradeUnitsPrecision()
    {
        return $this->tradeUnitsPrecision;
    }

    /**
     * Set minimumTradeSize
     *
     * @param float $minimumTradeSize
     *
     * @return Instrument
     */
    public function setMinimumTradeSize($minimumTradeSize)
    {
        $this->minimumTradeSize = $minimumTradeSize;

        return $this;
    }

    /**
     * Get minimumTradeSize
     *
     * @return float
     */
    public function getMinimumTradeSize()
    {
        return $this->minimumTradeSize;
    }

    /**
     * Set maximumTrailingStopDistance
     *
     * @param float $maximumTrailingStopDistance
     *
     * @return Instrument
     */
    public function setMaximumTrailingStopDistance($maximumTrailingStopDistance)
    {
        $this->maximumTrailingStopDistance = $maximumTrailingStopDistance;

        return $this;
    }

    /**
     * Get maximumTrailingStopDistance
     *
     * @return float
     */
    public function getMaximumTrailingStopDistance()
    {
        return $this->maximumTrailingStopDistance;
    }

    /**
     * Set minimumTrailingStopDistance
     *
     * @param float $minimumTrailingStopDistance
     *
     * @return Instrument
     */
    public function setMinimumTrailingStopDistance($minimumTrailingStopDistance)
    {
        $this->minimumTrailingStopDistance = $minimumTrailingStopDistance;

        return $this;
    }

    /**
     * Get minimumTrailingStopDistance
     *
     * @return float
     */
    public function getMinimumTrailingStopDistance()
    {
        return $this->minimumTrailingStopDistance;
    }

    /**
     * Set maximumPositionSize
     *
     * @param float $maximumPositionSize
     *
     * @return Instrument
     */
    public function setMaximumPositionSize($maximumPositionSize)
    {
        $this->maximumPositionSize = $maximumPositionSize;

        return $this;
    }

    /**
     * Get maximumPositionSize
     *
     * @return float
     */
    public function getMaximumPositionSize()
    {
        return $this->maximumPositionSize;
    }

    /**
     * Set maximumOrderUnits
     *
     * @param float $maximumOrderUnits
     *
     * @return Instrument
     */
    public function setMaximumOrderUnits($maximumOrderUnits)
    {
        $this->maximumOrderUnits = $maximumOrderUnits;

        return $this;
    }

    /**
     * Get maximumOrderUnits
     *
     * @return float
     */
    public function getMaximumOrderUnits()
    {
        return $this->maximumOrderUnits;
    }

    /**
     * Set marginRate
     *
     * @param float $marginRate
     *
     * @return Instrument
     */
    public function setMarginRate($marginRate)
    {
        $this->marginRate = $marginRate;

        return $this;
    }

    /**
     * Get marginRate
     *
     * @return float
     */
    public function getMarginRate()
    {
        return $this->marginRate;
    }
}

