<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="rate", indexes={
 *     @ORM\Index(name="moment_idx", columns={"moment"}),
 *     @ORM\Index(name="instrument_moment_idx", columns={"instrument_id","moment"}),
 *     @ORM\Index(name="instrument_moment_m60_idx", columns={"instrument_id","moment", "ma60"}),
 *     @ORM\Index(name="instrument_moment_w60_idx", columns={"instrument_id","moment", "wma60"}),
 *     @ORM\Index(name="instrument_moment_m75_idx", columns={"instrument_id","moment", "ma75"}),
 *     @ORM\Index(name="instrument_moment_w75_idx", columns={"instrument_id","moment", "wma75"}),
 *     @ORM\Index(name="instrument_moment_m90_idx", columns={"instrument_id","moment", "ma90"}),
 *     @ORM\Index(name="instrument_moment_w90_idx", columns={"instrument_id","moment", "wma90"}),
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RateRepository")
 */
class Rate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Instrument
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Instrument")
     */
    private $instrument;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="moment", type="datetime")
     */
    private $moment;

    /**
     * @var float
     *
     * @ORM\Column(name="bid", type="float")
     */
    private $bid;

    /**
     * @var float
     *
     * @ORM\Column(name="mid", type="float")
     */
    private $mid;

    /**
     * @var float
     *
     * @ORM\Column(name="ask", type="float")
     */
    private $ask;

    /**
     * @var float
     *
     * @ORM\Column(name="spread", type="float")
     */
    private $spread;

    /**
     * @var float
     *
     * @ORM\Column(name="spread_canonical", type="float")
     */
    private $spreadCanonical;

    /**
     * @var int
     *
     * @ORM\Column(name="volume", type="integer")
     */
    private $volume;

    /**
     * @var float
     *
     * @ORM\Column(name="ma60", type="float", nullable=true)
     */
    private $ma60;

    /**
     * @var float
     *
     * @ORM\Column(name="wma60", type="float", nullable=true)
     */
    private $wma60;

    /**
     * @var float
     *
     * @ORM\Column(name="ma75", type="float", nullable=true)
     */
    private $ma75;

    /**
     * @var float
     *
     * @ORM\Column(name="wma75", type="float", nullable=true)
     */
    private $wma75;

    /**
     * @var float
     *
     * @ORM\Column(name="ma90", type="float", nullable=true)
     */
    private $ma90;

    /**
     * @var float
     *
     * @ORM\Column(name="wma90", type="float", nullable=true)
     */
    private $wma90;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_m60", type="float", nullable=true)
     */
    private $trendM60;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_m75", type="float", nullable=true)
     */
    private $trendM75;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_m90", type="float", nullable=true)
     */
    private $trendM90;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_w60", type="float", nullable=true)
     */
    private $trendW60;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_w75", type="float", nullable=true)
     */
    private $trendW75;

    /**
     * @var float
     *
     * @ORM\Column(name="trend_w90", type="float", nullable=true)
     */
    private $trendW90;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set instrument
     *
     * @param Instrument $instrument
     *
     * @return Rate
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;

        return $this;
    }

    /**
     * Get instrument
     *
     * @return Instrument
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set moment
     *
     * @param \DateTime $moment
     *
     * @return Rate
     */
    public function setMoment($moment)
    {
        $this->moment = $moment;

        return $this;
    }

    /**
     * Get moment
     *
     * @return \DateTime
     */
    public function getMoment()
    {
        return $this->moment;
    }

    /**
     * Set bid
     *
     * @param float $bid
     *
     * @return Rate
     */
    public function setBid($bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get bid
     *
     * @return float
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set mid
     *
     * @param float $mid
     *
     * @return Rate
     */
    public function setMid($mid)
    {
        $this->mid = $mid;

        return $this;
    }

    /**
     * Get mid
     *
     * @return float
     */
    public function getMid()
    {
        return $this->mid;
    }

    /**
     * Set ask
     *
     * @param float $ask
     *
     * @return Rate
     */
    public function setAsk($ask)
    {
        $this->ask = $ask;

        return $this;
    }

    /**
     * Get ask
     *
     * @return float
     */
    public function getAsk()
    {
        return $this->ask;
    }

    /**
     * Set spread
     *
     * @param float $spread
     *
     * @return Rate
     */
    public function setSpread($spread)
    {
        $this->spread = $spread;

        // canconical spread set for EUR_USD instrument
        if(isset($this->instrument) and ($this->instrument instanceof Instrument)) {
            if ($this->instrument->getName() == 'EUR_USD') {
                $this->setSpreadCanonical(round($spread * 10000, 1));
            }
        }

        return $this;
    }

    /**
     * Get spread
     *
     * @return float
     */
    public function getSpread()
    {
        return $this->spread;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     *
     * @return Rate
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @return float
     */
    public function getSpreadCanonical(): float
    {
        return $this->spreadCanonical;
    }

    /**
     * @param float $spreadCanonical
     */
    public function setSpreadCanonical(float $spreadCanonical): void
    {
        $this->spreadCanonical = $spreadCanonical;
    }

    /**
     * @return float
     */
    public function getMa60(): float
    {
        return $this->ma60;
    }

    /**
     * @param float $ma60
     */
    public function setMa60(float $ma60): void
    {
        $this->ma60 = $ma60;
    }

    /**
     * @return float
     */
    public function getWma60(): float
    {
        return $this->wma60;
    }

    /**
     * @param float $wma60
     */
    public function setWma60(float $wma60): void
    {
        $this->wma60 = $wma60;
    }

    /**
     * @return float
     */
    public function getMa75(): float
    {
        return $this->ma75;
    }

    /**
     * @param float $ma75
     */
    public function setMa75(float $ma75): void
    {
        $this->ma75 = $ma75;
    }

    /**
     * @return float
     */
    public function getWma75(): float
    {
        return $this->wma75;
    }

    /**
     * @param float $wma75
     */
    public function setWma75(float $wma75): void
    {
        $this->wma75 = $wma75;
    }

    /**
     * @return float
     */
    public function getMa90(): float
    {
        return $this->ma90;
    }

    /**
     * @param float $ma90
     */
    public function setMa90(float $ma90): void
    {
        $this->ma90 = $ma90;
    }

    /**
     * @return float
     */
    public function getWma90(): float
    {
        return $this->wma90;
    }

    /**
     * @param float $wma90
     */
    public function setWma90(float $wma90): void
    {
        $this->wma90 = $wma90;
    }

    /**
     * @return float
     */
    public function getTrendM60(): float
    {
        return $this->trendM60;
    }

    /**
     * @param float $trendM60
     */
    public function setTrendM60(float $trendM60): void
    {
        $this->trendM60 = $trendM60;
    }

    /**
     * @return float
     */
    public function getTrendM75(): float
    {
        return $this->trendM75;
    }

    /**
     * @param float $trendM75
     */
    public function setTrendM75(float $trendM75): void
    {
        $this->trendM75 = $trendM75;
    }

    /**
     * @return float
     */
    public function getTrendM90(): float
    {
        return $this->trendM90;
    }

    /**
     * @param float $trendM90
     */
    public function setTrendM90(float $trendM90): void
    {
        $this->trendM90 = $trendM90;
    }

    /**
     * @return float
     */
    public function getTrendW60(): ?float
    {
        return $this->trendW60;
    }

    /**
     * @param float $trendW60
     */
    public function setTrendW60(float $trendW60): void
    {
        $this->trendW60 = $trendW60;
    }

    /**
     * @return float
     */
    public function getTrendW75(): float
    {
        return $this->trendW75;
    }

    /**
     * @param float $trendW75
     */
    public function setTrendW75(float $trendW75): void
    {
        $this->trendW75 = $trendW75;
    }

    /**
     * @return float
     */
    public function getTrendW90(): float
    {
        return $this->trendW90;
    }

    /**
     * @param float $trendW90
     */
    public function setTrendW90(float $trendW90): void
    {
        $this->trendW90 = $trendW90;
    }
}

