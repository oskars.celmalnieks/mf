<?php
/**
 * Created by PhpStorm.
 * User: oskars
 * Date: 18.4.9
 * Time: 14:22
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="api_env", type="integer", options={"default" : 0})
     */
    protected $apiEnv;

    /**
     * @ORM\Column(name="api_key", type="string", nullable=true)
     */
    protected $apiKey;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Account", mappedBy="user")
     */
    protected $accounts;

    public function __construct()
    {
        parent::__construct();

        $this->accounts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getApiEnv()
    {
        return $this->apiEnv;
    }

    /**
     * @param mixed $apiEnv
     */
    public function setApiEnv($apiEnv): void
    {
        $this->apiEnv = $apiEnv;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}