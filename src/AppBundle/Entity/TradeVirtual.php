<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TradeVirtual
 *
 * @ORM\Table(name="trade_virtual")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TradeVirtualRepository")
 */
class TradeVirtual
{
    CONST STATE_TRADE_VIRTUAL_OPEN = 1;
    CONST STATE_TRADE_VIRTUAL_CLOSE = 2;

    CONST SIDE_TRADE_VIRTUAL_LONG = 1;
    CONST SIDE_TRADE_VIRTUAL_SHORT = 2;

    CONST SCENARIO_TRADE_VIRTUAL_EUR_USD = 77;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Instrument
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Instrument")
     */
    private $instrument;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_trade_open", type="datetime")
     */
    private $timeTradeOpen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_trade_close", type="datetime", nullable=true)
     */
    private $timeTradeClose;

    /**
     * @var int
     *
     * @ORM\Column(name="scenario_id", type="integer")
     */
    private $scenarioId;

    /**
     * @var int
     *
     * @ORM\Column(name="units", type="integer")
     */
    private $units;

    /**
     * @var float
     *
     * @ORM\Column(name="price_open", type="float")
     */
    private $priceOpen;

    /**
     * @var float
     *
     * @ORM\Column(name="price_close", type="float", nullable=true)
     */
    private $priceClose;

    /**
     * @var float
     *
     * @ORM\Column(name="realized_pl", type="float", nullable=true)
     */
    private $realizedPl;

    /**
     * @var int
     *
     * @ORM\Column(name="side", type="smallint")
     */
    private $side;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="smallint")
     */
    private $state;

    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="float", nullable=true)
     */
    private $balance;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set instrumentId
     *
     * @param integer $instrument
     *
     * @return TradeVirtual
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;

        return $this;
    }

    /**
     * Get instrumentId
     *
     * @return int
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set timeTradeOpen
     *
     * @param \DateTime $timeTradeOpen
     *
     * @return TradeVirtual
     */
    public function setTimeTradeOpen($timeTradeOpen)
    {
        $this->timeTradeOpen = $timeTradeOpen;

        return $this;
    }

    /**
     * Get timeTradeOpen
     *
     * @return \DateTime
     */
    public function getTimeTradeOpen()
    {
        return $this->timeTradeOpen;
    }

    /**
     * Set timeTradeClose
     *
     * @param \DateTime $timeTradeClose
     *
     * @return TradeVirtual
     */
    public function setTimeTradeClose($timeTradeClose)
    {
        $this->timeTradeClose = $timeTradeClose;

        return $this;
    }

    /**
     * Get timeTradeClose
     *
     * @return \DateTime
     */
    public function getTimeTradeClose()
    {
        return $this->timeTradeClose;
    }

    /**
     * Set scenarioId
     *
     * @param integer $scenarioId
     *
     * @return TradeVirtual
     */
    public function setScenarioId($scenarioId)
    {
        $this->scenarioId = $scenarioId;

        return $this;
    }

    /**
     * Get scenarioId
     *
     * @return int
     */
    public function getScenarioId()
    {
        return $this->scenarioId;
    }

    /**
     * Set units
     *
     * @param integer $units
     *
     * @return TradeVirtual
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * Get units
     *
     * @return int
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * Set priceOpen
     *
     * @param float $priceOpen
     *
     * @return TradeVirtual
     */
    public function setPriceOpen($priceOpen)
    {
        $this->priceOpen = $priceOpen;

        return $this;
    }

    /**
     * Get priceOpen
     *
     * @return float
     */
    public function getPriceOpen()
    {
        return $this->priceOpen;
    }

    /**
     * Set priceClose
     *
     * @param float $priceClose
     *
     * @return TradeVirtual
     */
    public function setPriceClose($priceClose)
    {
        $this->priceClose = $priceClose;

        return $this;
    }

    /**
     * Get priceClose
     *
     * @return float
     */
    public function getPriceClose()
    {
        return $this->priceClose;
    }

    /**
     * Set realizedPl
     *
     * @param float $realizedPl
     *
     * @return TradeVirtual
     */
    public function setRealizedPl($realizedPl)
    {
        $this->realizedPl = $realizedPl;

        return $this;
    }

    /**
     * Get realizedPl
     *
     * @return float
     */
    public function getRealizedPl()
    {
        return $this->realizedPl;
    }

    /**
     * Set side
     *
     * @param integer $side
     *
     * @return TradeVirtual
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }

    /**
     * Get side
     *
     * @return int
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return TradeVirtual
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance(float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }
}

