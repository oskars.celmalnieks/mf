<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\CreatedUpdatedTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * AccountData
 *
 * @ORM\Table(name="account_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountDataRepository")
 * @ORM\HasLifecycleCallbacks
 */
class AccountData
{
    use CreatedUpdatedTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="guaranteed_stop_loss_order_mode", type="string")
     */
    protected $guaranteedStopLossOrderMode;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Account", mappedBy="accountData")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $account;

    /**
     * @var Scenario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Scenario")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $scenario;

    /**
     * @ORM\Column(name="margin_rate", type="float")
     */
    protected $marginRate; // float
    
    /**
     * @ORM\Column(name="hedging_enabled", type="boolean")
     */protected $hedgingEnabled;

    /**
     * @ORM\Column(name="last_transaction_id", type="integer")
     */
    protected $lastTransactionID; // int

    /**
     * @ORM\Column(name="balance", type="float")
     */
    protected $balance; //float

    /**
     * @ORM\Column(name="open_trade_count", type="float")
     */
    protected $openTradeCount; //int

    /**
     * @ORM\Column(name="open_position_count", type="integer")
     */
    protected $openPositionCount; //int

    /**
     * @ORM\Column(name="pending_order_count", type="integer")
     */
    protected $pendingOrderCount; //int

    /**
     * @ORM\Column(name="pl", type="float")
     */
    protected $pl; //float

    /**
     * @ORM\Column(name="resettable_pl", type="float")
     */
    protected $resettablePL; //float

    /**
     * @ORM\Column(name="financing", type="float")
     */
    protected $financing; //float

    /**
     * @ORM\Column(name="commission", type="float")
     */
    protected $commission; //float

    /**
     * @ORM\Column(name="guaranteed_execution_fees", type="float")
     */
    protected $guaranteedExecutionFees; //float

    /**
     * @ORM\Column(name="unrealized_pl", type="float")
     */
    protected $unrealizedPL; //float

    /**
     * @ORM\Column(name="nav", type="float")
     */
    protected $nav; //float

    /**
     * @ORM\Column(name="margin_used", type="float")
     */
    protected $marginUsed; //float

    /**
     * @ORM\Column(name="margin_available", type="float")
     */
    protected $marginAvailable; //float

    /**
     * @ORM\Column(name="position_value", type="float")
     */
    protected $positionValue; //float

    /**
     * @ORM\Column(name="margin_closeout_unrealized_pl", type="float")
     */
    protected $marginCloseoutUnrealizedPL; //float

    /**
     * @ORM\Column(name="margin_closeout_nav", type="float")
     */
    protected $marginCloseoutNAV; //float
    
    /**
     * @ORM\Column(name="margin_closeout_margin_used", type="float")
     */
    protected $marginCloseoutMarginUsed; //float

    /**
     * @ORM\Column(name="margin_closeout_position_value", type="float")
     */
    protected $marginCloseoutPositionValue; //float
    
    /**
     * @ORM\Column(name="margin_closeout_percent", type="float")
     */
    protected $marginCloseoutPercent; //float
    
    /**
     * @ORM\Column(name="withdrawal_limit", type="float")
     */
    protected $withdrawalLimit; //float
    
    /**
     * @ORM\Column(name="margin_call_margin_used", type="float")
     */
    protected $marginCallMarginUsed; //float

    /**
     * @ORM\Column(name="margin_call_percent", type="float")
     */
    protected $marginCallPercent;

    /**
     * @ORM\Column(name="alias", type="string")
     */
    protected $alias;

    /**
     * @return mixed
     */
    public function getGuaranteedStopLossOrderMode()
    {
        return $this->guaranteedStopLossOrderMode;
    }

    /**
     * @param mixed $guaranteedStopLossOrderMode
     * @return AccountData
     */
    public function setGuaranteedStopLossOrderMode($guaranteedStopLossOrderMode)
    {
        $this->guaranteedStopLossOrderMode = $guaranteedStopLossOrderMode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     * @return AccountData
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginRate()
    {
        return $this->marginRate;
    }

    /**
     * @param mixed $marginRate
     * @return AccountData
     */
    public function setMarginRate($marginRate)
    {
        $this->marginRate = $marginRate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHedgingEnabled()
    {
        return $this->hedgingEnabled;
    }

    /**
     * @param mixed $hedgingEnabled
     * @return AccountData
     */
    public function setHedgingEnabled($hedgingEnabled)
    {
        $this->hedgingEnabled = $hedgingEnabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastTransactionID()
    {
        return $this->lastTransactionID;
    }

    /**
     * @param mixed $lastTransactionID
     * @return AccountData
     */
    public function setLastTransactionID($lastTransactionID)
    {
        $this->lastTransactionID = $lastTransactionID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return AccountData
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpenTradeCount()
    {
        return $this->openTradeCount;
    }

    /**
     * @param mixed $openTradeCount
     * @return AccountData
     */
    public function setOpenTradeCount($openTradeCount)
    {
        $this->openTradeCount = $openTradeCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpenPositionCount()
    {
        return $this->openPositionCount;
    }

    /**
     * @param mixed $openPositionCount
     * @return AccountData
     */
    public function setOpenPositionCount($openPositionCount)
    {
        $this->openPositionCount = $openPositionCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPendingOrderCount()
    {
        return $this->pendingOrderCount;
    }

    /**
     * @param mixed $pendingOrderCount
     * @return AccountData
     */
    public function setPendingOrderCount($pendingOrderCount)
    {
        $this->pendingOrderCount = $pendingOrderCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPl()
    {
        return $this->pl;
    }

    /**
     * @param mixed $pl
     * @return AccountData
     */
    public function setPl($pl)
    {
        $this->pl = $pl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResettablePL()
    {
        return $this->resettablePL;
    }

    /**
     * @param mixed $resettablePL
     * @return AccountData
     */
    public function setResettablePL($resettablePL)
    {
        $this->resettablePL = $resettablePL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinancing()
    {
        return $this->financing;
    }

    /**
     * @param mixed $financing
     * @return AccountData
     */
    public function setFinancing($financing)
    {
        $this->financing = $financing;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     * @return AccountData
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuaranteedExecutionFees()
    {
        return $this->guaranteedExecutionFees;
    }

    /**
     * @param mixed $guaranteedExecutionFees
     * @return AccountData
     */
    public function setGuaranteedExecutionFees($guaranteedExecutionFees)
    {
        $this->guaranteedExecutionFees = $guaranteedExecutionFees;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnrealizedPL()
    {
        return $this->unrealizedPL;
    }

    /**
     * @param mixed $unrealizedPL
     * @return AccountData
     */
    public function setUnrealizedPL($unrealizedPL)
    {
        $this->unrealizedPL = $unrealizedPL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNav()
    {
        return $this->nav;
    }

    /**
     * @param mixed $nav
     * @return AccountData
     */
    public function setNav($nav)
    {
        $this->nav = $nav;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginUsed()
    {
        return $this->marginUsed;
    }

    /**
     * @param mixed $marginUsed
     * @return AccountData
     */
    public function setMarginUsed($marginUsed)
    {
        $this->marginUsed = $marginUsed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginAvailable()
    {
        return $this->marginAvailable;
    }

    /**
     * @param mixed $marginAvailable
     * @return AccountData
     */
    public function setMarginAvailable($marginAvailable)
    {
        $this->marginAvailable = $marginAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPositionValue()
    {
        return $this->positionValue;
    }

    /**
     * @param mixed $positionValue
     * @return AccountData
     */
    public function setPositionValue($positionValue)
    {
        $this->positionValue = $positionValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCloseoutUnrealizedPL()
    {
        return $this->marginCloseoutUnrealizedPL;
    }

    /**
     * @param mixed $marginCloseoutUnrealizedPL
     * @return AccountData
     */
    public function setMarginCloseoutUnrealizedPL($marginCloseoutUnrealizedPL)
    {
        $this->marginCloseoutUnrealizedPL = $marginCloseoutUnrealizedPL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCloseoutNAV()
    {
        return $this->marginCloseoutNAV;
    }

    /**
     * @param mixed $marginCloseoutNAV
     * @return AccountData
     */
    public function setMarginCloseoutNAV($marginCloseoutNAV)
    {
        $this->marginCloseoutNAV = $marginCloseoutNAV;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCloseoutMarginUsed()
    {
        return $this->marginCloseoutMarginUsed;
    }

    /**
     * @param mixed $marginCloseoutMarginUsed
     * @return AccountData
     */
    public function setMarginCloseoutMarginUsed($marginCloseoutMarginUsed)
    {
        $this->marginCloseoutMarginUsed = $marginCloseoutMarginUsed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCloseoutPositionValue()
    {
        return $this->marginCloseoutPositionValue;
    }

    /**
     * @param mixed $marginCloseoutPositionValue
     * @return AccountData
     */
    public function setMarginCloseoutPositionValue($marginCloseoutPositionValue)
    {
        $this->marginCloseoutPositionValue = $marginCloseoutPositionValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCloseoutPercent()
    {
        return $this->marginCloseoutPercent;
    }

    /**
     * @param mixed $marginCloseoutPercent
     * @return AccountData
     */
    public function setMarginCloseoutPercent($marginCloseoutPercent)
    {
        $this->marginCloseoutPercent = $marginCloseoutPercent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalLimit()
    {
        return $this->withdrawalLimit;
    }

    /**
     * @param mixed $withdrawalLimit
     * @return AccountData
     */
    public function setWithdrawalLimit($withdrawalLimit)
    {
        $this->withdrawalLimit = $withdrawalLimit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCallMarginUsed()
    {
        return $this->marginCallMarginUsed;
    }

    /**
     * @param mixed $marginCallMarginUsed
     * @return AccountData
     */
    public function setMarginCallMarginUsed($marginCallMarginUsed)
    {
        $this->marginCallMarginUsed = $marginCallMarginUsed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarginCallPercent()
    {
        return $this->marginCallPercent;
    }

    /**
     * @param mixed $marginCallPercent
     * @return AccountData
     */
    public function setMarginCallPercent($marginCallPercent)
    {
        $this->marginCallPercent = $marginCallPercent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return Scenario
     */
    public function getScenario(): Scenario
    {
        return $this->scenario;
    }

    /**
     * @param Scenario $scenario
     */
    public function setScenario(Scenario $scenario): self
    {
        $this->scenario = $scenario;
        return $this;
    }
}

