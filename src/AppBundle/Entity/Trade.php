<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\CreatedUpdatedTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trade
 *
 * @ORM\Table(name="trade")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TradeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Trade
{
    CONST TRADE_STATE_OPEN = 1;
    CONST TRADE_STATE_CLOSED = 2;

    use CreatedUpdatedTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @var Instrument
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Instrument")
     * @ORM\JoinColumn(nullable=false)
     */

    private $instrument;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_order_submit", type="datetime")
     */
    private $timeOrderSubmit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_order_fill", type="datetime", nullable=true)
     */
    private $timeOrderFill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_trade_close", type="datetime", nullable=true)
     */
    private $timeTradeClose;

    /**
     * @var float
     *
     * @ORM\Column(name="units", type="float")
     */
    private $units;

    /**
     * @var float
     *
     * @ORM\Column(name="current_units", type="float")
     */
    private $currentUnits;

    /**
     * @var float
     *
     * @ORM\Column(name="account_balance", type="float", nullable=true)
     */
    private $accountBalance;

    /**
     * @var int
     *
     * @ORM\Column(name="order_id", type="integer", nullable=true)
     */
    private $orderId;

    /**
     * @var float
     *
     * @ORM\Column(name="pl", type="float", nullable=true)
     */
    private $pl;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="oanda_trade_id", type="integer")
     */
    private $oandaTradeId;

    /**
     * @var array
     *
     * @ORM\Column(name="related_transactions", type="array", nullable=true)
     */
    private $relatedTransactions;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var float
     *
     * @ORM\Column(name="realized_pl", type="float", nullable=true)
     */
    private $realizedPl;

    /**
     * @var float
     *
     * @ORM\Column(name="unrealized_pl", type="float", nullable=true)
     */
    private $unrealizedPl;

    /**
     * @var float
     *
     * @ORM\Column(name="last_price", type="float", nullable=true)
     */
    private $lastPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="commission", type="float", nullable=true)
     */
    private $commission;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", nullable=true)
     */
    private $comment;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return Trade
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set instrument
     *
     * @param Instrument $instrument
     *
     * @return Trade
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;

        return $this;
    }

    /**
     * Get instrument
     *
     * @return Instrument
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set timeOrderSubmit
     *
     * @param \DateTime $timeOrderSubmit
     *
     * @return Trade
     */
    public function setTimeOrderSubmit($timeOrderSubmit)
    {
        $this->timeOrderSubmit = $timeOrderSubmit;

        return $this;
    }

    /**
     * Get timeOrderSubmit
     *
     * @return \DateTime
     */
    public function getTimeOrderSubmit()
    {
        return $this->timeOrderSubmit;
    }

    /**
     * Set timeOrderFill
     *
     * @param \DateTime $timeOrderFill
     *
     * @return Trade
     */
    public function setTimeOrderFill($timeOrderFill)
    {
        $this->timeOrderFill = $timeOrderFill;

        return $this;
    }

    /**
     * Get timeOrderFill
     *
     * @return \DateTime
     */
    public function getTimeOrderFill()
    {
        return $this->timeOrderFill;
    }

    /**
     * Set units
     *
     * @param integer $units
     *
     * @return Trade
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * Get units
     *
     * @return int
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * Set accountBalance
     *
     * @param float $accountBalance
     *
     * @return Trade
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return float
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return Trade
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set pl
     *
     * @param float $pl
     *
     * @return Trade
     */
    public function setPl($pl)
    {
        $this->pl = $pl;

        return $this;
    }

    /**
     * Get pl
     *
     * @return float
     */
    public function getPl()
    {
        return $this->pl;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Trade
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return Trade
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Set oandaTradeId
     *
     * @param integer $oandaTradeId
     *
     * @return Trade
     */
    public function setOandaTradeId($oandaTradeId)
    {
        $this->oandaTradeId = $oandaTradeId;

        return $this;
    }

    /**
     * Get oandaTradeId
     *
     * @return int
     */
    public function getOandaTradeId()
    {
        return $this->oandaTradeId;
    }

    /**
     * Set orderFillType
     *
     * @param string $orderFillType
     *
     * @return Trade
     */
    public function setOrderFillType($orderFillType)
    {
        $this->orderFillType = $orderFillType;

        return $this;
    }

    /**
     * Set relatedTransactions
     *
     * @param array $relatedTransactions
     *
     * @return Trade
     */
    public function setRelatedTransactions($relatedTransactions)
    {
        $this->relatedTransactions = $relatedTransactions;

        return $this;
    }

    /**
     * Get relatedTransactions
     *
     * @return array
     */
    public function getRelatedTransactions()
    {
        return $this->relatedTransactions;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return float
     */
    public function getCurrentUnits(): float
    {
        return $this->currentUnits;
    }

    /**
     * @param float $currentUnits
     */
    public function setCurrentUnits(float $currentUnits): self
    {
        $this->currentUnits = $currentUnits;

        return $this;
    }

    /**
     * @return float
     */
    public function getRealizedPl(): float
    {
        return $this->realizedPl;
    }

    /**
     * @param float $realizedPl
     */
    public function setRealizedPl(float $realizedPl): self
    {
        $this->realizedPl = $realizedPl;

        return $this;
    }

    /**
     * @return float
     */
    public function getUnrealizedPl(): ?float
    {
        return $this->unrealizedPl;
    }

    /**
     * @param float $unrealizedPl
     */
    public function setUnrealizedPl(float $unrealizedPl): self
    {
        $this->unrealizedPl = $unrealizedPl;

        return $this;
    }

    /**
     * @return float
     */
    public function getLastPrice(): float
    {
        return $this->lastPrice;
    }

    /**
     * @param float $lastPrice
     */
    public function setLastPrice(float $lastPrice): self
    {
        $this->lastPrice = $lastPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getCommission(): float
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getTimeTradeClose(): \DateTime
    {
        return $this->timeTradeClose;
    }

    /**
     * @param \DateTime $timeTradeClose
     */
    public function setTimeTradeClose(\DateTime $timeTradeClose): void
    {
        $this->timeTradeClose = $timeTradeClose;
    }
}

