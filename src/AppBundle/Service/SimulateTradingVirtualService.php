<?php
/**
 * Created by PhpStorm.
 * User: ocelmalnieks
 * Date: 4/18/17
 * Time: 11:43 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\Trade;
use AppBundle\Entity\TradeVirtual;
use AppBundle\Entity\User;
use AppBundle\Repository\TradeVirtualRepository;
use Doctrine\ORM\EntityManager;

class SimulateTradingVirtualService
{
    private $spreadProfit;
    private $spreadLoss;

    private $units;
    private $maCount;

    private $spread;
    private $profitRatio;

    private $volume;
    private $scenario;

    CONST BUFFER_FOR_ACTUAL_TRADES = 0.0001;

    /**
     * @var Rate[]
     */
    private $ratesArrayAsc;

    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @var OandaOperationsService $oandaService
     */
    private $oandaService;

    /**
     * SimulateTrade constructor.
     */
    public function __construct(EntityManager $em, OandaOperationsService $oandaService)
    {
        $this->em = $em;
        $this->oandaService = $oandaService;
    }

    public function init(array $passedData, array $limits)
    {
        $this->ratesArrayAsc = $passedData['ratesObjectArray'];
        $this->units = $passedData['units'];

        $this->maCount = $passedData['instrumentSpecifics']['maCount'];
        $this->spread = $passedData['instrumentSpecifics']['spread'];
        $this->profitRatio = $passedData['instrumentSpecifics']['profitRatio'];
        $this->volume = $passedData['instrumentSpecifics']['volume'];

        $this->spreadProfit = $limits['spreadProfit'];
        $this->spreadLoss = $limits['spreadLoss'];
        $this->scenario = $passedData['scenario'];
    }

    public function getMA50($count){

        $ma = $this->maCount;

        if($count > $ma){

            $mediumBucket = 0;
            $ma50Array = array_slice($this->ratesArrayAsc, $count-$ma, $ma);

            foreach ($ma50Array as $maMediums){

                /** @var Rate $maMediums */
                $mediumBucket += $maMediums->getMid();
            }

            $ma50 = (double) $mediumBucket / $ma;

            return round($ma50, 4);

        } else {

            return false;
        }
    }

    function trendDirection(array $values) {

        $x_sum = array_sum(array_keys($values));
        $y_sum = array_sum($values);
        $meanX = $x_sum / count($values);
        $meanY = $y_sum / count($values);

        // calculate sums
        $mBase = $mDivisor = 0.0;

        foreach($values as $i => $value) {
            $mBase += ($i - $meanX) * ($value - $meanY);
            $mDivisor += ($i - $meanX) * ($i - $meanX);
        }

        // calculate slope
        $slope = $mBase / $mDivisor;

        if($slope <= 0){
            $direction = 'down';
        } else {
            $direction = 'up';
        };

        return [$direction, round($slope * 100000,2)];
    }

    function isLessThanLastX(array $values, $lastMedium) {

        $isLess = true;

        foreach ($values as $eachValue){
            if ($eachValue->getMid() < $lastMedium){
                return false;
            }
        }

        return $isLess;
    }

    function isGraterThanLastX(array $values, $lastMedium) {

        $isGrater = true;

        foreach ($values as $eachValue){
            if ($eachValue->getMid() > $lastMedium){
                return false;
            }
        }

        return $isGrater;
    }


    public function execute($setMaxPipResult = false){

        $total      = count($this->ratesArrayAsc);

        /** @var TradeVirtualRepository $virtualTradesRepo */
        $virtualTradesRepo = $this->em->getRepository(TradeVirtual::class);

        $i = $total - 1;

            $timeNow = $this->ratesArrayAsc[$i]->getMoment();
            $day = date('D', date_timestamp_get($timeNow));

            //****************
            // close one of two
            //****************

        $activeTrades = $this->getAllActiveTrades();

        if(count($activeTrades) == 2){

            // get LONG active trade
            /** @var TradeVirtual | null $trade */
            $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_LONG);

            if($this->ratesArrayAsc[$i]->getBid() >= $trade->getPriceOpen())
                {
                    $closeRate = $this->ratesArrayAsc[$i]->getBid();
                    $net = round(($closeRate - $trade->getPriceOpen())*10000, 2);

                    $this->closeAndFlushThisTrade($trade, $net, $i, $closeRate);
                }

                $activeTrades = $this->getAllActiveTrades();

                if(count($activeTrades) == 2){

                    // get SHORT active trade
                    /** @var TradeVirtual $trade */
                    $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_SHORT);

                    if($this->ratesArrayAsc[$i]->getAsk() <= $trade->getPriceOpen()){

                        $closeRate = $this->ratesArrayAsc[$i]->getAsk();
                        $net = round(($trade->getPriceOpen() - $closeRate) * 10000, 2);

                        $this->closeAndFlushThisTrade($trade, $net, $i, $closeRate);
                    }
                }
            }

            //****************
            // checks for positive scenario
            //****************

            $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_LONG);

            if(
                is_null($trade)
                && round($this->ratesArrayAsc[$i-3]->getMid(), 4) > round($this->ratesArrayAsc[$i-2]->getMid(), 4)
                && round($this->ratesArrayAsc[$i-2]->getMid(), 4) > round($this->ratesArrayAsc[$i-1]->getMid(), 4)
                && round($this->ratesArrayAsc[$i-1]->getMid(), 4) > round($this->ratesArrayAsc[$i]->getMid(), 4)
                && $this->ratesArrayAsc[$i]->getSpreadCanonical() < $this->spread
                && $this->ratesArrayAsc[$i]->getVolume() > $this->volume
            )
            {
                $this->openLong($i);

                $tradePot = $this->getAllActiveTrades();
                if(count($tradePot) > 1){

                    // close short trade
                    $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_SHORT);
                    $closeRate = $this->ratesArrayAsc[$i]->getAsk();

                    $net = round(($trade->getPriceOpen() - $closeRate) * 10000, 2);
                    $this->closeAndFlushThisTrade($trade, $net, $i, $closeRate);
                }
            }

//            ****************
//             checks for negative scenario
//            ****************
            $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_SHORT);

            if (
                is_null($trade)
                && round($this->ratesArrayAsc[$i - 3]->getMid(),4) < round($this->ratesArrayAsc[$i - 2]->getMid(),4)
                && round($this->ratesArrayAsc[$i - 2]->getMid(),4) < round($this->ratesArrayAsc[$i - 1]->getMid(),4)
                && round($this->ratesArrayAsc[$i - 1]->getMid(),4) < round($this->ratesArrayAsc[$i]->getMid(),4)
                && $this->ratesArrayAsc[$i]->getSpreadCanonical() < $this->spread
                && $this->ratesArrayAsc[$i]->getVolume() > $this->volume
            ) {
                $this->openShort($i);

                $tradePot = $this->getAllActiveTrades();
                if(count($tradePot) == 2){

                    // close long trade
                    $trade = $this->getOneOrNullActiveTradeBySide(TradeVirtual::SIDE_TRADE_VIRTUAL_LONG);

                    $closeRate = $this->ratesArrayAsc[$i]->getBid();
                    $net = round(($closeRate - $trade->getPriceOpen())*10000, 2);

                    $this->closeAndFlushThisTrade($trade, $net, $i, $closeRate);
                }
            }

//            if($setMaxPipResult) {
//                if ($subTotal > 320 ) {
//                    $resultData = [
//                        'tradeFlow' => $tradeFlow,
//                        'subTotal' => $subTotal,
//                    ];
//
//                    return $resultData;
//                }
//            }
    }

    public function getOneOrNullActiveTradeBySide($side)
    {
        /** @var TradeVirtualRepository $virtualTradesRepo */
        $virtualTradesRepo = $this->em->getRepository(TradeVirtual::class);


        /** @var TradeVirtual $trade */
        $trade = $virtualTradesRepo->findOneBy(
            [
                'side' => $side,
                'state' => TradeVirtual::STATE_TRADE_VIRTUAL_OPEN,
            ]
        );

        return $trade;

    }

    public function getAllActiveTrades()
    {
        /** @var TradeVirtualRepository $virtualTradesRepo */
        $virtualTradesRepo = $this->em->getRepository(TradeVirtual::class);

        $activeTrades = $virtualTradesRepo->findBy(
            [
                'scenarioId' => $this->scenario,
                'state' => TradeVirtual::STATE_TRADE_VIRTUAL_OPEN,
            ]
        );

        return $activeTrades;

    }

    public function closeAndFlushThisTrade(TradeVirtual $trade, $net, $i, $closeRate)
    {
        $trade
            ->setState(TradeVirtual::STATE_TRADE_VIRTUAL_CLOSE)
            ->setRealizedPl($net)
            ->setTimeTradeClose($this->ratesArrayAsc[$i]->getMoment())
            ->setPriceClose($closeRate)
        ;

        $this->em->persist($trade);
        $this->em->flush($trade);


        $this->executeRealClose($trade->getSide());
    }

    public function checkDayAndSubtotal($day, $subTotal){

        if($day != 'Fri'){

            return true;

        } else {

            if ($subTotal > 90) {
                return false;
            } else {
                return true;
            }
        }

    }

    public function isActiveTypeInsidePot(array $tradePot, string $type): bool {

        if(!empty($tradePot)) {
            foreach ($tradePot as $trade) {
                if (($trade['isActive']) && ($trade['type'] == $type)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function tradeOpenTimeStop(\DateTime $timeNow, \DateTime $tradeOpenTime): bool
    {
        $HOUR_1 = 3600;
        $HOUR_3 = 11800;
        $HOUR_6 = 23600;

        $timeStampNow = $timeNow->getTimestamp();
        $timeStampOpen = $tradeOpenTime->getTimestamp();

        $diff = $timeStampNow - $timeStampOpen;

        if ($diff >= $HOUR_3*2.5){

            return true;
        }

        return false;
    }

    public function getTradeByType(array $tradePot, string $type){

        foreach ($tradePot as $index => $trade){
            if($trade['type'] == $type){
                return [ 'trade' => $trade, 'index' => $index];
            }
        }

        return false;
    }

    /**
     * @param $i
     */
    public function openShort($i)
    {
        $tradeVirtual = new TradeVirtual();
        $tradeVirtual
            ->setState(TradeVirtual::STATE_TRADE_VIRTUAL_OPEN)
            ->setSide(TradeVirtual::SIDE_TRADE_VIRTUAL_SHORT)
            ->setPriceOpen($this->ratesArrayAsc[$i]->getBid() + self::BUFFER_FOR_ACTUAL_TRADES)
            ->setTimeTradeOpen($this->ratesArrayAsc[$i]->getMoment())
            ->setUnits($this->units)
            ->setScenarioId($this->scenario)
        ;

        $this->em->persist($tradeVirtual);
        $this->em->flush($tradeVirtual);

        $this->executeRealTrades(Account::SIDE_SHORT);
    }

    /**
     * @param $i
     */
    public function openLong($i)
    {
        $tradeVirtual = new TradeVirtual();
        $tradeVirtual
            ->setState(TradeVirtual::STATE_TRADE_VIRTUAL_OPEN)
            ->setSide(TradeVirtual::SIDE_TRADE_VIRTUAL_LONG)
            ->setPriceOpen($this->ratesArrayAsc[$i]->getAsk() - self::BUFFER_FOR_ACTUAL_TRADES)
            ->setTimeTradeOpen($this->ratesArrayAsc[$i]->getMoment())
            ->setUnits($this->units)
            ->setScenarioId($this->scenario)
        ;

        $this->em->persist($tradeVirtual);
        $this->em->flush($tradeVirtual);

        $this->executeRealTrades(Account::SIDE_LONG);
    }

    public function executeRealTrades($side){

        $userRepo = $this->em->getRepository(User::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        $accountRepo = $this->em->getRepository(Account::class);
        $instrumentRepo = $this->em->getRepository(Instrument::class);

        $instrument = $instrumentRepo->findOneBy(['name' => Instrument::DEFAULT_INSTRUMENT_NAME]);
        $account = $accountRepo->findOneBy(['user' => $thisUser, 'side' => $side, 'enabled' => true]);

        /** @var AccountData $accountData */
        $accountData = $account->getAccountData();

        $this->oandaService
            ->initUser($thisUser)
            ->initOandaV20()
            ->openTrade($instrument, $account->getUnits());
    }

    public function executeRealClose($side)
    {

        $userRepo = $this->em->getRepository(User::class);
        $thisUser = $userRepo->findOneBy(['username' => 'oskars']);

        $accountRepo = $this->em->getRepository(Account::class);
        $instrumentRepo = $this->em->getRepository(Instrument::class);

        $instrument = $instrumentRepo->findOneBy(['name' => Instrument::DEFAULT_INSTRUMENT_NAME]);
        $account = $accountRepo->findOneBy(['user' => $thisUser, 'side' => $side, 'enabled' => true]);

        $tradeRepo = $this->em->getRepository(Trade::class);
        $trade = $tradeRepo->findOneBy(['account' => $account, 'state' => Trade::TRADE_STATE_OPEN]);

        $this->oandaService->closeTrade($account, $trade);
    }
}