<?php
/**
 * Created by PhpStorm.
 * User: ocelmalnieks
 * Date: 8/25/17
 * Time: 9:19 AM
 */

namespace AppBundle\Service;


use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;

class GenerateMissingWmaService
{
    private $maCount;

    private $em;

    /**
     * GenerateMissingWmas constructor.
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function setMaCount(int $maCount){

        $this->maCount = $maCount;
    }

    public function updateMissgin(){

        $ratesRepo = $this->em->getRepository('AppBundle:Rate');
        $instrument = $this->em->getRepository(Instrument::class)->findOneBy(['name'=>Instrument::DEFAULT_INSTRUMENT_NAME]);

        $startDate = new \DateTime('2017-01-01');

        $allRateObjects = $ratesRepo->getAllRateObjectsWithoutDirectionWma($instrument, $this->maCount, $startDate);
        $updateCounter = 0;

        foreach ($allRateObjects as $rateObject){

            /**
             * @var Rate $rateObject
             */

            $thisMoment = $rateObject->getMoment();

            $lastXMediumArray = $this->em->getRepository('AppBundle:Rate')->getLastXmidsBelowDate($this->maCount, $instrument, $thisMoment);

            $setMaCount = sprintf('setMa%s', (string) $this->maCount);
            $setWmaCount = sprintf('setWma%s', (string) $this->maCount);

            $rateObject->{$setMaCount}($this->calculateMA($lastXMediumArray));
            $rateObject->{$setWmaCount}($this->calculateWMA($lastXMediumArray));
            $this->em->persist($rateObject);

            $updateCounter++;
//            dump($updateCounter);
//            $rateObject->setDirection($this->trendDirection($thisInstrument, 'ma', $thisMoment));
//            $rateObject->setDirectionWma($this->trendDirection($thisInstrument, 'wma', $thisMoment));

            if($updateCounter > 1000) {

                $this->em->flush();
                $updateCounter = 0;

                $message = sprintf('updated 1k at, preset time: %s', $rateObject->getMoment()->format('Y-m-d H:i:s'));
                dump($message);
            }
        }

        $this->em->flush();
    }

    public function updateMissginTrendDirection(){

        $ratesRepo = $this->em->getRepository('AppBundle:Rate');
        $instrument = $this->em->getRepository(Instrument::class)->findOneBy(['name'=>Instrument::DEFAULT_INSTRUMENT_NAME]);

        $startDate = new \DateTime('2017-01-04');

        $allRateObjects = $ratesRepo->getAllRateObjectsWithoutDirectionWma($instrument, $this->maCount, $startDate, true);
        $updateCounter = 0;

        dump(count($allRateObjects));

        foreach ($allRateObjects as $rateObject){

            /**
             * @var Rate $rateObject
             */

            $thisMoment = $rateObject->getMoment();

//            dump($thisMoment);
//            $lastXMediumArray = $this->em->getRepository('AppBundle:Rate')->getLastXmidsBelowDate($this->maCount, $instrument, $thisMoment);

            $setMTrend = sprintf('setTrendM%s', (string) $this->maCount);
            $setWTrend = sprintf('setTrendW%s', (string) $this->maCount);

//            dump($setWTrend);
//            dump($setWTrend);

            $multiplier = 1000;

            $mTrendSlope = (float) $this->trendDirection($instrument, 'ma', $thisMoment);
            $wTrendSlope = (float) $this->trendDirection($instrument, 'wma', $thisMoment);

            $rateObject->{$setMTrend}($mTrendSlope * $multiplier);
            $rateObject->{$setWTrend}($wTrendSlope * $multiplier);

            $this->em->persist($rateObject);

            $updateCounter++;
//            $message = sprintf('preset time updating: %s', $rateObject->getMoment()->format('Y-m-d H:i:s'));
//            dump($message);

            if($updateCounter > 1000) {

                $this->em->flush();
                $updateCounter = 0;

                $message = sprintf('present time updating: %s', $rateObject->getMoment()->format('Y-m-d H:i:s'));
                dump($message);
            }
        }

        $this->em->flush();
    }

    public function calculateMA($lastXMediumArray){

        $mediumBucket = 0;

        foreach ($lastXMediumArray as $maMedium){

            /** @var Rate $maMediums */
            $mediumBucket += $maMedium;
        }

        $maX = (double) $mediumBucket / $this->maCount;

        return round($maX, 5);
    }

    public function calculateWMA($lastXMediumArray){

        $ascXMediumArray = array_reverse($lastXMediumArray);

        $wma = 0;
        $triangular = 0;

        for( $i = 1; $i <= $this->maCount; $i++){
            $triangular += $i;
        }

        $index = 1;
        foreach ($ascXMediumArray as $medium){

            $wma += $medium * ($index/$triangular);
            $index++;
        }

        return round($wma, 5);
    }

    function trendDirection($instrument, $type, $date) {

        if ($type == 'wma'){
            $values = $this->em->getRepository('AppBundle:Rate')->getLastXWMAsBelowDate($this->maCount, $instrument, $date);
        }
        else {
            $values = $this->em->getRepository('AppBundle:Rate')->getLastXMAsBelowDate($this->maCount, $instrument, $date);
        }

        if(count($values) == 0){
            return 0;
        }

        $x_sum = array_sum(array_keys($values));
        $y_sum = array_sum($values);
        $meanX = $x_sum / count($values);
        $meanY = $y_sum / count($values);

        // calculate sums
        $mBase = $mDivisor = 0.0;

        foreach($values as $i => $value) {
            $mBase += ($i - $meanX) * ($value - $meanY);
            $mDivisor += ($i - $meanX) * ($i - $meanX);
        }

        // calculate slope
        if($mDivisor != 0) {
            $slope = $mBase / $mDivisor;
        } else{
            $slope = 0;
        }

        return $slope;
    }
}