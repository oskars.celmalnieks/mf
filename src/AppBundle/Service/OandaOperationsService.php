<?php
/**
 * Created by PhpStorm.
 * User: oskars
 * Date: 18.11.9
 * Time: 10:12
 */

namespace AppBundle\Service;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountData;
use AppBundle\Entity\Instrument;
use AppBundle\Entity\Rate;
use AppBundle\Entity\Trade;
use AppBundle\Entity\User;
use AppBundle\Repository\AccountDataRepository;
use AppBundle\Repository\AccountRepository;
use AppBundle\Repository\InstrumentRepository;
use AppBundle\Repository\RateRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use TheCodeMill\OANDA\OANDAv20;

class OandaOperationsService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var User|string
     */
    private $user;

    /**
     * @var Logger
     */
    private $monolog;

    /**
     * @var OANDAv20 $oandaService
     */
    private $oandaService;

    /**
     * OandaOperationsService constructor.
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        ContainerInterface $container
    )
    {
        $this->em = $em;
        $this->user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;
        $this->monolog = $container->get('monolog.logger.db');
    }

    private function getRepoForClass($class)
    {

        return $this->em->getRepository(get_class($class));
    }

    public function initUser(User $user = null): self
    {

        if (!$this->user instanceof User) {
            $this->user = $user;
        }

        return $this;
    }

    public function initOandaV20(): self
    {

        if ($this->oandaService == null) {
            $this->oandaService = new OANDAv20($this->user->getApiEnv(), $this->user->getApiKey());
        }

        return $this;
    }

    public function fetchCurrentUserAccounts(): self
    {
        $accounts = $this->oandaService->getAccounts();

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->em->getRepository(Account::class);

        foreach ($accounts['accounts'] as $account) {

            if ($accountRepo->findOneBy(['oandaId' => $account['id']])) {
                continue;
            }

            $thisAccount = new Account();
            $thisAccount
                ->setOandaId($account['id'])
                ->setEnabled(false)
                ->setUser($this->user)
                ->setSide(Account::SIDE_NONE);

            $this->em->persist($thisAccount);
        }

        $this->em->flush();

        return $this;
    }

    public function importAllTradingInstruments(): self
    {

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->em->getRepository(Account::class);

        /** @var Account $firstAccountOfTheUser */
        $firstAccountOfTheUser = $accountRepo->findOneBy(['user' => $this->user]);
        $accountInfo = $this->oandaService->getAccountInstruments($firstAccountOfTheUser->getOandaId());

        $instrumentRepo = $this->em->getRepository(Instrument::class);

        foreach ($accountInfo['instruments'] as $instrument) {

            if ($instrumentRepo->findOneBy(['name' => $instrument['name']])) {
                continue;
            }

            $thisInstrument = new Instrument();
            foreach ($instrument as $parameter => $value) {

                $setter = 'set' . $parameter;
                if (method_exists($thisInstrument, $setter)) {
                    $thisInstrument->{$setter}($value);
                }
            }

            $this->em->persist($thisInstrument);
            $this->monolog->info(sprintf('Instrument added: %s', $instrument['name']), ['category' => 'Import instruments']);
        }

        $this->em->flush();
        $this->monolog->info(sprintf('Instrument import finished'), ['category' => 'Import instruments']);

        return $this;
    }

    public function getAllAccountSummaryForCurrentUser(): self
    {

        /** @var AccountDataRepository $accountDataRepo */
        $accountDataRepo = $this->em->getRepository(AccountData::class);

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->em->getRepository(Account::class);

        $accounts = $accountRepo->findBy(['user' => $this->user]);
        foreach ($accounts as $account) {

            /** @var Account $account */
            $accountSummary = $this->oandaService->getAccountSummary($account->getOandaId());
            $accountData = $accountDataRepo->findOneBy(['account' => $account]);

            if (is_null($accountData)) {

                $accountData = new AccountData();
                $accountData->setAccount($account);
                $account->setAccountData($accountData);
            }

            foreach ($accountSummary['account'] as $parameter => $value) {

                $setter = 'set' . $parameter;
                if (method_exists($accountData, $setter)) {
                    $accountData->{$setter}($value);
                }
            }

            $this->em->persist($accountData);
            $this->em->persist($account);

            $this->monolog->info(sprintf('Account data imported for: %s', $account->getOandaId()), ['category' => 'Account summary']);
        }

        $this->em->flush();

        return $this;
    }

    public function getInstrumentRates(array $data = null, Instrument $instrument = null): self
    {
        if (is_null($data)) {

            $data = [
                'count' => 5000,
                'price' => 'MBA',
                'granularity' => 'M1',
            ];
        }

        /** @var InstrumentRepository $instrumentRepo */
        $instrumentRepo = $this->em->getRepository(Instrument::class);

        if (is_null($instrument)) {
            $instrument = $instrumentRepo->findOneBy(['name' => Instrument::DEFAULT_INSTRUMENT_NAME]);
        }

        /** @var RateRepository $rateRepo */
        $rateRepo = $this->em->getRepository(Rate::class);

        $instrumentData = $this->oandaService->getInstrumentCandles($instrument->getName(), $data);

        /** @var Instrument $thisInstrument */
        $thisInstrument = $instrumentRepo->findOneBy(['name' => $instrumentData['instrument']]);

        $ratesSaved = 0;
        $counter = 0;

        foreach ($instrumentData['candles'] as $priceArray) {

            if (!$priceArray['complete']) {
                continue;
            }

            $thisDate = $priceArray['time'];
            $onlyTime = str_replace('T', ' ', explode('.', $thisDate)[0]);

            $date_obj = new \DateTime($onlyTime, new \DateTimeZone('UTC'));
            $thisRate = $rateRepo->findOneBy(['moment' => $date_obj]);

            if (!is_null($thisRate)) {
                continue;
            }

            $spread = $priceArray['ask']['c'] - $priceArray['bid']['c'];

            $thisRate = new Rate();
            $thisRate->setMoment($date_obj)
                ->setInstrument($thisInstrument)
                ->setAsk($priceArray['ask']['c'])
                ->setMid($priceArray['mid']['c'])
                ->setBid($priceArray['bid']['c'])
                ->setSpread($spread)
                ->setVolume($priceArray['volume']);

            $this->em->persist($thisRate);
            $ratesSaved++;
            $counter++;

            if ($counter > 100) {

                $this->em->flush();
                $counterMessage = sprintf('Rates saved counter: %d Flushed: %d', $ratesSaved, $counter);

                echo $counterMessage . PHP_EOL;
                $counter = 0;
            }
        }

        $this->em->flush();
        $this->monolog->info(sprintf('Rates saved total count: %d', $ratesSaved), ['category' => 'Import rates']);

        return $this;
    }

    public function getAllOpenTrades()
    {
        $this->checkUser();

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->em->getRepository(Account::class);
        $allAccounts = $accountRepo->findBy(['user' => $this->user]);

        if (empty($allAccounts)) {

            $message = 'No accounts for current user';

            return [
                'status' => 'error',
                'message' => $message,
                'data' => null,
            ];
        }

        $this->monolog->info(sprintf('Open trades fetching for user %d started.', $this->user->getId()));

        $allTrades = [];
        foreach ($allAccounts as $account) {

            /** @var Account $account */
            $openTrades = $this->oandaService->getOpenTrades($account->getOandaId());
            array_merge($allTrades, $openTrades);

            $this->saveUpdateTradesRecieved($account, $openTrades);
        }

        $this->monolog->info(sprintf('Open trades fetching for user %d finished.', $this->user->getId()));

        return [
            'status' => 'success',
            'message' => 'Open trades fetching complete',
            'data' => [
                'openTrades' => $allTrades,
            ],
        ];
    }

    private function saveUpdateTradesRecieved(Account $account, array $openTrades): void
    {
        if (count($openTrades['trades']) == 0) {
            return;
        }

        $tradesRepo = $this->em->getRepository(Trade::class);

        $newTradesCount = 0;
        $updatedTradesCount = 0;

        // iterate through recieved open trades
        foreach ($openTrades['trades'] as $openTrade) {

            $thisTradeObject = $tradesRepo->findOneBy(['oandaTradeId' => $openTrade['id']]);
            if (is_null($thisTradeObject)) {

                $thisTradeObject = new Trade();
                $thisTradeObject->setOandaTradeId($openTrade['id']);
                $thisTradeObject->setInstrument($account->getInstrument());
                $thisTradeObject->setAccount($account);
                $thisTradeObject->setTimeOrderFill($this->getTimeObjectFromString($openTrade['openTime']));
                $thisTradeObject->setTimeOrderSubmit($this->getTimeObjectFromString($openTrade['openTime']));
                $thisTradeObject->setUnits($openTrade['initialUnits']);
                $thisTradeObject->setState(Trade::TRADE_STATE_OPEN);
                $thisTradeObject->setPrice($openTrade['price']);
                $thisTradeObject->setComment('Trade added manually from Oanda');

                $this->monolog->info(sprintf('Trade object created with OandaTradeId: %d', $openTrade['id']));
                $newTradesCount++;

            } else {
                $updatedTradesCount++;
            }

            $thisTradeObject->setCurrentUnits($openTrade['currentUnits']);
            $thisTradeObject->setRealizedPl($openTrade['realizedPL']);
            $thisTradeObject->setUnrealizedPl($openTrade['unrealizedPL']);

            $this->em->persist($thisTradeObject);
        }

        $this->monolog->info(
            sprintf('Trade objects for customer id: %d  updated: %d and created: : %d',
                $account->getUser()->getId(),
                $updatedTradesCount,
                $newTradesCount
            )
        );

        $this->em->flush();
        $this->monolog->info(sprintf('Trade objects for user $d flushed to db', $account->getUser()->getId()));
    }

    /**
     * @param $instrument
     * @param $units
     * @return string
     */
    public function openTrade($instrument, $units): array
    {
        if (is_null($this->user)) {

            $userRepo = $this->em->getRepository(User::class);
            $this->user = $userRepo->findOneBy(['username' => 'oskars']);
            echo 'Setting default user oskars';
        }

        if (is_null($this->user)) {

            echo(sprintf("User not found"));
            $message = "User not found";

            return [
                'status' => 'error',
                'message' => $message,
                'data' => null,
            ];
        }

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->em->getRepository(Account::class);
        /** @var InstrumentRepository $instrumentRepo */
        $instrumentRepo = $this->em->getRepository(Instrument::class);

        /** @var Instrument $thisInstrument */
        $thisInstrument = $instrumentRepo->findOneBy(
            [
                'name' => trim(strtoupper($instrument))
            ]
        );

        if (is_null($thisInstrument)) {
            $thisInstrument = $instrumentRepo->findOneBy(
                [
                    'name' => Instrument::DEFAULT_INSTRUMENT_NAME
                ]
            );
        }

        if ($units > 0) {
            $side = Account::SIDE_LONG;
        } else {
            $side = Account::SIDE_SHORT;
        }

        /** @var Account $accountToOperate */
        $accountToOperate = $accountRepo->findOneBy(
            [
                'user' => $this->user,
                'side' => $side,
                'enabled' => true,
                'instrument' => $thisInstrument
            ]
        );

        $oandaService = new OANDAv20($this->user->getApiEnv(), $this->user->getApiKey());
        $orderData =
            [
                "order" =>
                    [
                        "units" => (string)$units,
                        "instrument" => $thisInstrument->getName(),
                        "timeInForce" => "FOK",
                        "type" => "MARKET",
                        "positionFill" => "DEFAULT",
                    ],
            ];

        /** @var Response $tradeOpenedObject */
        $tradeOpenedObject = $oandaService->createOrder($accountToOperate->getOandaId(), $orderData);
        $tradeOpened = json_decode($tradeOpenedObject->getBody()->getContents(), TRUE);

        if ($tradeOpenedObject->getStatusCode() !== 201) {
            $this->monolog->alert('Trade opening failed', (array)$tradeOpened);
        }

        $thisTrade = new Trade();
        $thisTrade
            ->setOandaTradeId($tradeOpened['orderFillTransaction']['tradeOpened']['tradeID'])
            ->setUnits($tradeOpened['orderFillTransaction']['tradeOpened']['units'])
            ->setCurrentUnits($tradeOpened['orderFillTransaction']['tradeOpened']['units'])
            ->setInstrument($thisInstrument)
            ->setReason($tradeOpened['orderFillTransaction']['reason'])
            ->setTimeOrderSubmit($this->getTimeObjectFromString($tradeOpened['orderCreateTransaction']['time']))
            ->setTimeOrderFill($this->getTimeObjectFromString($tradeOpened['orderFillTransaction']['time']))
            ->setOrderFillType($tradeOpened['orderCreateTransaction']['reason'])
            ->setAccountBalance($tradeOpened['orderFillTransaction']['accountBalance'])
            ->setPl(($tradeOpened['orderFillTransaction']['pl']))
            ->setRelatedTransactions($tradeOpened['relatedTransactionIDs'])
            ->setAccount($accountToOperate)
            ->setOrderId($tradeOpened['orderCreateTransaction']['id'])
            ->setPrice($tradeOpened['orderFillTransaction']['price'])
            ->setState(Trade::TRADE_STATE_OPEN)
            ->setCommission($tradeOpened['orderFillTransaction']['commission']);

        $this->em->persist($thisTrade);

        $this->em->flush();

        echo(
        sprintf(
            'Trade opened successfully :: instrument: %s units: %d' . PHP_EOL,
            $thisTrade->getInstrument()->getName(),
            $thisTrade->getUnits()
        )
        );

        $tradeObjectArray = null;
        $this->objToArray($thisTrade, $tradeObjectArray);

        $this->monolog->info('Trade opened', $tradeObjectArray);
        $message = 'Finished' . PHP_EOL;

        return [
            'status' => 'success',
            'message' => $message,
            'data' => [
                'trade' => $tradeObjectArray,
            ],
        ];
    }

    public function closeTrade(Account $account, Trade $trade)
    {

        $this->checkUser();
        $oandaService = new OANDAv20($this->user->getApiEnv(), $this->user->getApiKey());

        $data = [
            'units' => 'ALL',
        ];

        try {
            $tradeCloseAction = $oandaService->closeTrade($account->getOandaId(), $trade->getOandaTradeId(), $data);
        } catch (RequestException $e) {

            $tradeJson = json_encode((array)$trade);
            $this->monolog->error('Trade close ERROR', ['message' => $e->getMessage(), 'response' => $e->getResponse()->getReasonPhrase(), 'trade' => $tradeJson]);

            return [
                'status' => 'error',
                'message' => $e->getMessage(),
                'data' => [
                    'trade' => $tradeJson,
                ],
            ];
        }

        $closedTrade = json_decode($tradeCloseAction->getBody()->getContents(), TRUE);
        $closeData = $closedTrade['orderFillTransaction'];

        $trade->setTimeTradeClose($this->getTimeObjectFromString($closeData['time']));
        $trade->setState(Trade::TRADE_STATE_CLOSED);
        $trade->setLastPrice($closeData['price']);
        $trade->setPl($closeData['pl']);
        $trade->setRealizedPl($closeData['pl']);

        $trade->setAccountBalance($closedTrade['orderFillTransaction']['accountBalance']);

        $this->em->flush($trade);

        $tradeObjectArray = null;
        $this->objToArray($trade, $tradeObjectArray);

        $this->monolog->info('Trade closed', ['oanda_trade_id' => $trade->getOandaTradeId(), 'oanda_account_id' => $trade->getAccount()->getOandaId(), 'pl' => $trade->getPl()]);
        $message = 'Finished' . PHP_EOL;

        return [
            'status' => 'success',
            'message' => $message,
            'data' => [
                'trade' => $tradeObjectArray,
            ],
        ];
    }

    private function getTimeObjectFromString($timeString)
    {

        $onlyTime = str_replace('T', ' ', explode('.', $timeString)[0]);
        $date_obj = new \DateTime($onlyTime, new \DateTimeZone('UTC'));

        return $date_obj;
    }

    private function objToArray($obj, &$arr)
    {

        if (!is_object($obj) && !is_array($obj)) {
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value) {
            if (!empty($value)) {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            } else {
                $arr[$key] = $value;
            }
        }

        return $arr;
    }

    private function checkUser()
    {
        if (is_null($this->user)) {

            $userRepo = $this->em->getRepository(User::class);
            $this->user = $userRepo->findOneBy(['username' => 'oskars']);
            echo 'Setting default user oskars';
        }

        if (is_null($this->user)) {

            echo(sprintf("User not found"));
            $message = "User not found";

            return [
                'status' => 'error',
                'message' => $message,
                'data' => null,
            ];
        }
    }
}