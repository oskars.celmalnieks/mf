<?php
/**
 * Created by PhpStorm.
 * User: ocelmalnieks
 * Date: 4/18/17
 * Time: 11:43 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Rate;

class SimulateTradingService
{
    private $spreadProfit;
    private $spreadLoss;

    private $units;
    private $maCount;

    private $spread;
    private $profitRatio;

    private $volume;

    CONST BUFFER_FOR_ACTUAL_TRADES = 0.0000;

    /**
     * @var array Rate
     */
    private $ratesArrayAsc;

    /**
     * SimulateTrade constructor.
     */
    public function __construct()
    {
    }

    public function init(array $passedData, array $limits)
    {
        $this->ratesArrayAsc = $passedData['ratesObjectArray'];
        $this->units = $passedData['units'];

        $this->maCount = $passedData['instrumentSpecifics']['maCount'];
        $this->spread = $passedData['instrumentSpecifics']['spread'];
        $this->profitRatio = $passedData['instrumentSpecifics']['profitRatio'];
        $this->volume = $passedData['instrumentSpecifics']['volume'];

        $this->spreadProfit = $limits['spreadProfit'];
        $this->spreadLoss = $limits['spreadLoss'];
    }

    public function getMA50($count){

        $ma = $this->maCount;

        if($count > $ma){

            $mediumBucket = 0;
            $ma50Array = array_slice($this->ratesArrayAsc, $count-$ma, $ma);

            foreach ($ma50Array as $maMediums){

                /** @var Rate $maMediums */
                $mediumBucket += $maMediums->getMid();
            }

            $ma50 = (double) $mediumBucket / $ma;

            return round($ma50, 4);

        } else {

            return false;
        }
    }

    function trendDirection(array $values) {

        $x_sum = array_sum(array_keys($values));
        $y_sum = array_sum($values);
        $meanX = $x_sum / count($values);
        $meanY = $y_sum / count($values);

        // calculate sums
        $mBase = $mDivisor = 0.0;

        foreach($values as $i => $value) {
            $mBase += ($i - $meanX) * ($value - $meanY);
            $mDivisor += ($i - $meanX) * ($i - $meanX);
        }

        // calculate slope
        $slope = $mBase / $mDivisor;

        if($slope <= 0){
            $direction = 'down';
        } else {
            $direction = 'up';
        };

        return [$direction, round($slope * 100000,2)];
    }

    function isLessThanLastX(array $values, $lastMedium) {

        $isLess = true;

        foreach ($values as $eachValue){
            if ($eachValue->getMid() < $lastMedium){
                return false;
            }
        }

        return $isLess;
    }

    function isGraterThanLastX(array $values, $lastMedium) {

        $isGrater = true;

        foreach ($values as $eachValue){
            if ($eachValue->getMid() > $lastMedium){
                return false;
            }
        }

        return $isGrater;
    }


    public function seeCommonResult($setMaxPipResult = false){

        $tradeFlow  = [];
        $tradePot   = [];

        $total      = count($this->ratesArrayAsc);
        $subTotal   = 0;
        $last       = 10;

        $closedOpositeShortTrades = 0;
        $closedOpositeLongTrades = 0;

        $tradeList = [];

//        dump(count($this->ratesArrayAsc));

        for($i = 5; $i <= $total - $last; $i++) {

            $timeNow = $this->ratesArrayAsc[$i]->getMoment();
            $day = date('D', date_timestamp_get($timeNow));

            //****************
            // close one of two
            //****************

            if(count($tradePot) == 2){

                $tradeData = $this->getTradeByType($tradePot, 'long');
                $trade = $tradeData['trade'];

                if($this->ratesArrayAsc[$i]->getBid() >= $trade['openRate'])
                {
                    $trade['closeRate'] = $this->ratesArrayAsc[$i]->getBid();
                    $trade['net'] = round(($trade['closeRate'] - $trade['openRate'])*10000, 2);

                    $subTotal = round($subTotal + $trade['net'], 2);
                    $timeDiff = $this->ratesArrayAsc[$i]->getMoment()->diff($trade['openMoment']);

                    $tradeFlow[] = $subTotal;
                    unset($tradePot[$tradeData['index']]);

                    $closedOpositeLongTrades += 1;
                }

                if(count($tradePot) == 2){

                    $tradeData = $this->getTradeByType($tradePot, 'short');
                    $trade = $tradeData['trade'];

                    if($this->ratesArrayAsc[$i]->getAsk() <= $trade['openRate']){

                        $trade['closeRate'] = $this->ratesArrayAsc[$i]->getAsk();
                        $trade['net'] = round(($trade['openRate'] - $trade['closeRate']) * 10000, 2);

                        $subTotal = round($subTotal + $trade['net'], 2);

                        $tradeFlow[] = $subTotal;
                        unset($tradePot[$tradeData['index']]);

                        $closedOpositeShortTrades += 1;
                    }
                }
            }

            //****************
            // close short positions
            //****************

            if ($this->isActiveTypeInsidePot($tradePot, 'short')) {

                $tradeData = $this->getTradeByType($tradePot, 'short');
                $trade = $tradeData['trade'];

                if ((
                        $this->ratesArrayAsc[$i]->getAsk() > $trade['closeNegative']
                        || $this->ratesArrayAsc[$i]->getAsk() < $trade['closePositive']
                    )
                    || ($i == $total - $last)
                ) {

                    $trade['closeRate'] = $this->ratesArrayAsc[$i]->getAsk();
                    $trade['net'] = round(($trade['openRate'] - $trade['closeRate']) * 10000, 2);

                    $subTotal = round($subTotal + $trade['net'], 2);

                    $tradeFlow[] = $subTotal;
                    unset($tradePot[$tradeData['index']]);

                    $closedOpositeLongTrades = 0;
                }
            }

            //****************
            // close long positions
            //****************

            if($this->isActiveTypeInsidePot($tradePot, 'long')){

                $tradeData = $this->getTradeByType($tradePot, 'long');
                $trade = $tradeData['trade'];

                if((
                        $this->ratesArrayAsc[$i]->getBid() > $trade['closePositive']
                        || $this->ratesArrayAsc[$i]->getBid() < $trade['closeNegative']
                    )
                    || ($i == $total-$last)
                )

                {
                    $trade['closeRate'] = $this->ratesArrayAsc[$i]->getBid();
                    $trade['net'] = round(($trade['closeRate'] - $trade['openRate'])*10000, 2);

                    $subTotal = round($subTotal + $trade['net'], 2);
                    $timeDiff = $this->ratesArrayAsc[$i]->getMoment()->diff($trade['openMoment']);

                    $tradeFlow[] = $subTotal;
                    unset($tradePot[$tradeData['index']]);

                    $closedOpositeShortTrades = 0;
                }
            }

            //****************
            // checks for positive scenario
            //****************

            if(
                !$this->isActiveTypeInsidePot($tradePot, 'long')
                && round($this->ratesArrayAsc[$i-3]->getMid(), 4) > round($this->ratesArrayAsc[$i-2]->getMid(), 4)
                && round($this->ratesArrayAsc[$i-2]->getMid(), 4) > round($this->ratesArrayAsc[$i-1]->getMid(), 4)
                && round($this->ratesArrayAsc[$i-1]->getMid(), 4) > round($this->ratesArrayAsc[$i]->getMid(), 4)
                && $this->ratesArrayAsc[$i]->getSpreadCanonical() < $this->spread
                && $this->ratesArrayAsc[$i]->getVolume() > $this->volume
            )
            {
                $this->openLong($i, $tradePot, $trade);
                $tradeList[] = $trade;

                if(count($tradePot) > 1){

                    // close short trade
                    $tradeData = $this->getTradeByType($tradePot, 'short');
                    $trade = $tradeData['trade'];

                    $trade['closeRate'] = $this->ratesArrayAsc[$i]->getAsk();
                    $trade['net'] = round(($trade['openRate'] - $trade['closeRate']) * 10000, 2);

                    $subTotal = round($subTotal + $trade['net'], 2);

                    $tradeFlow[] = $subTotal;
                    unset($tradePot[$tradeData['index']]);
                }
            }

//            ****************
//             checks for negative scenario
//            ****************

            if (
                !$this->isActiveTypeInsidePot($tradePot, 'short')
                && round($this->ratesArrayAsc[$i - 3]->getMid(),4) < round($this->ratesArrayAsc[$i - 2]->getMid(),4)
                && round($this->ratesArrayAsc[$i - 2]->getMid(),4) < round($this->ratesArrayAsc[$i - 1]->getMid(),4)
                && round($this->ratesArrayAsc[$i - 1]->getMid(),4) < round($this->ratesArrayAsc[$i]->getMid(),4)
                && $this->ratesArrayAsc[$i]->getSpreadCanonical() < $this->spread
                && $this->ratesArrayAsc[$i]->getVolume() > $this->volume
            ) {
                $this->openShort($i, $tradePot, $trade);
                $tradeList[] = $trade;

                if(count($tradePot) == 2){

                    // close long trade
                    $tradeData = $this->getTradeByType($tradePot, 'long');
                    $trade = $tradeData['trade'];

                    $trade['closeRate'] = $this->ratesArrayAsc[$i]->getBid();
                    $trade['net'] = round(($trade['closeRate'] - $trade['openRate'])*10000, 2);

                    $subTotal = round($subTotal + $trade['net'], 2);
                    $timeDiff = $this->ratesArrayAsc[$i]->getMoment()->diff($trade['openMoment']);

                    $tradeFlow[] = $subTotal;
                    unset($tradePot[$tradeData['index']]);
                }
            }

            if($setMaxPipResult) {
                if ($subTotal > 320 ) {
                    $resultData = [
                        'tradeFlow' => $tradeFlow,
                        'subTotal' => $subTotal,
                    ];

//                    dump($tradeList);
                    return $resultData;
                }
            }
        }

        $resultData = [
            'tradeFlow' => $tradeFlow,
            'subTotal' => $subTotal,
        ];

//        dump($tradeList);

        return $resultData;
    }

    public function checkDayAndSubtotal($day, $subTotal){

        if($day != 'Fri'){

            return true;

        } else {

            if ($subTotal > 90) {
                return false;
            } else {
                return true;
            }
        }

    }

    public function isActiveTypeInsidePot(array $tradePot, string $type): bool {

        if(!empty($tradePot)) {
            foreach ($tradePot as $trade) {
                if (($trade['isActive']) && ($trade['type'] == $type)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function tradeOpenTimeStop(\DateTime $timeNow, \DateTime $tradeOpenTime): bool
    {
        $HOUR_1 = 3600;
        $HOUR_3 = 11800;
        $HOUR_6 = 23600;

        $timeStampNow = $timeNow->getTimestamp();
        $timeStampOpen = $tradeOpenTime->getTimestamp();

        $diff = $timeStampNow - $timeStampOpen;

        if ($diff >= $HOUR_3*2.5){

            return true;
        }

        return false;
    }

    public function getTradeByType(array $tradePot, string $type){

        foreach ($tradePot as $index => $trade){
            if($trade['type'] == $type){
                return [ 'trade' => $trade, 'index' => $index];
            }
        }

        return false;
    }

    /**
     * @param $i
     * @param $tradePot
     * @param $trade
     */
    public function openShort($i, &$tradePot, &$trade)
    {
        $trade = [];

        $trade['isActive'] = true;
        $trade['type'] = 'short';
        $trade['openRate'] = $this->ratesArrayAsc[$i]->getBid() + self::BUFFER_FOR_ACTUAL_TRADES;
        $trade['closeNegative'] = round($this->ratesArrayAsc[$i]->getBid() + $this->spreadLoss, 5);
        $trade['closePositive'] = round($this->ratesArrayAsc[$i]->getBid() - $this->spreadProfit, 5);
        $trade['openPosition'] = $this->ratesArrayAsc[$i]->getBid() * $this->units;
        $trade['openMoment'] = $this->ratesArrayAsc[$i]->getMoment();

        $tradePot[] = $trade;
    }

    /**
     * @param $i
     * @param $tradePot
     * @param $trade
     */
    public function openLong($i, &$tradePot, &$trade)
    {
        $trade = [];

        $trade['isActive'] = true;
        $trade['type'] = 'long';
        $trade['openRate'] = $this->ratesArrayAsc[$i]->getAsk() - self::BUFFER_FOR_ACTUAL_TRADES;
        $trade['closePositive'] = round($this->ratesArrayAsc[$i]->getAsk() + $this->spreadProfit, 5);
        $trade['closeNegative'] = round($this->ratesArrayAsc[$i]->getAsk() - $this->spreadLoss, 5);
        $trade['openPosition'] = $this->ratesArrayAsc[$i]->getAsk() * $this->units;
        $trade['openMoment'] = $this->ratesArrayAsc[$i]->getMoment();

        $tradePot[] = $trade;
    }
}