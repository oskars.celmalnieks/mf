<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\Rate;
use AppBundle\Entity\Trade;
use AppBundle\Entity\TradeVirtual;
use AppBundle\Entity\User;
use AppBundle\Service\OandaOperationsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $tradeRepo = $this->getDoctrine()->getRepository(Trade::class);
        $virtualTradeRepo = $this->getDoctrine()->getRepository(TradeVirtual::class);

        $sumTrade = $tradeRepo->sumTradeList(Trade::TRADE_STATE_CLOSED, $user);
        $sumTradeVirtual = $virtualTradeRepo->sumVirtualTradeList(TradeVirtual::STATE_TRADE_VIRTUAL_CLOSE);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig',
            [
                'sumTradeReal' => $sumTrade,
                'sumTradeVirtual' => $sumTradeVirtual,
            ]
        );
    }

    /**
     * @Route("/list-trades", name="list_trades")
     */
    public function listTradesAction(Request $request)
    {
        if ($request->isMethod('POST')) {

            $keys_must_have = ['account', 'trade_id', 'action'];
            $data = $request->request->all();

            $keys_ok = $this->array_keys_exists($keys_must_have, $data);

            if($keys_ok && $data['action'] == 'close') {

                $em = $this->getDoctrine();

                $accountRepo = $em->getRepository(Account::class);
                $thisAccount = $accountRepo->findOneBy([
                    'id' => $data['account'],
                    'user' => $this->getUser()
                ]);

                $tradeRepo = $em->getRepository(Trade::class);
                $thisTrade = $tradeRepo->findOneBy([
                        'account' => $thisAccount,
                        'oandaTradeId' => $data['trade_id']
                    ]
                );

                if(!is_null($thisTrade)) {

                    /** @var OandaOperationsService $oandaService */
                    $oandaService = $this->get(OandaOperationsService::class);

                    $result = $oandaService
                        ->initUser($this->getUser())
                        ->initOandaV20()
                        ->closeTrade($thisAccount, $thisTrade);
                    ;

                    if($result['status'] == 'success') {
                        $this->addFlash("success", "Trade closed");
                    }

                    if($result['status'] == 'error') {
                        $this->addFlash("danger", "Error: ".$result['message']);
                    }
                }

            } else {
                $this->addFlash("warning", "Request parameters missing");
            }
        }

        $LAST_TRADE_COUNT = 10;

        /** @var User $user */
        $user = $this->getUser();
        $tradeRepo = $this->getDoctrine()->getRepository(Trade::class);
        $rateRepo = $this->getDoctrine()->getRepository(Rate::class);

        $openTradesLast = $tradeRepo->getLastTradeList($LAST_TRADE_COUNT, Trade::TRADE_STATE_OPEN, $user);
        $closedTradesLast = $tradeRepo->getLastTradeList($LAST_TRADE_COUNT, Trade::TRADE_STATE_CLOSED, $user);

        $lastRate = $rateRepo->getLastRate();

        return $this->render('default/tradesList.html.twig', [
            'openTrades' => $openTradesLast,
            'closedTrades' => $closedTradesLast,
            'lastRate' => $lastRate,
        ]);
    }

    function array_keys_exists(array $keys, array $arr) {
        return !array_diff_key(array_flip($keys), $arr);
    }

    /**
     * @Route("/compare-trades", name="compare_trades")
     */
    public function compareTradesAction(Request $request)
    {
        $LAST_TRADE_COUNT = 20;

        /** @var User $user */
        $user = $this->getUser();

        $tradeRepo = $this->getDoctrine()->getRepository(Trade::class);
        $virtualTradeRepo = $this->getDoctrine()->getRepository(TradeVirtual::class);
        $rateRepo = $this->getDoctrine()->getRepository(Rate::class);

        $openTradesLast = $tradeRepo->getLastTradeList($LAST_TRADE_COUNT, Trade::TRADE_STATE_OPEN, $user);

        $closedTradesLast = $tradeRepo->getLastTradeList($LAST_TRADE_COUNT, Trade::TRADE_STATE_CLOSED, $user);

        /** @var TradeVirtual[] $closedVirtualLast */
        $closedVirtualLast = $virtualTradeRepo->getLastVirtualTradeList($LAST_TRADE_COUNT, TradeVirtual::STATE_TRADE_VIRTUAL_CLOSE, TradeVirtual::SCENARIO_TRADE_VIRTUAL_EUR_USD);

        $lastRate = $rateRepo->getLastRate();

        return $this->render('default/tradesCompare.html.twig', [
            'openTrades' => $openTradesLast,
            'closedTrades' => $closedTradesLast,
            'closedVirtualLast' => $closedVirtualLast,
            'lastRate' => $lastRate,
        ]);
    }
}